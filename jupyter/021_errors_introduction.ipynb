{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " - Content is distributed with a Creative Commons Attribution license, CC-BY \n",
    " - Code is distributed with a MIT license\n",
    " \n",
    "![creative commons attribution](./images/CC-BY.png)\n",
    "\n",
    " - (c) Kyle T. Mandli\n",
    " - (c) Pablo Angulo y Fabricio Macià"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerial methods\n",
    "\n",
    "A __numerical method__ is a procedure that approximates the solution of a mathematical problem:\n",
    "  - Not all problems admit exact solutions.\n",
    "  - Symbolic calculus cannot always find the exact solution, even if it exists.\n",
    "  - Even if the solution exists, finding it can be too expensive.\n",
    "  - Many methods for finding exact solutions do not offer warranties that they eill finish in a reasonable amount of time.\n",
    "\n",
    "In general, a numerical method must be able to approximate the solution to the mathematical problem with as much precision as we need.\n",
    "In practice, too much precision is unnecesary, and costful.\n",
    "So all good numerical methods must strike a balance between:\n",
    "  - small error\n",
    "  - small computing time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Types of error\n",
    "\n",
    " - Errors in data: __measurement errors__, __noise__...\n",
    " - Errors due to a simplification of the reality by a mathematical model: __model error__.\n",
    " - Errors due to replacing a mathematical object of interest (number, vector, function ...) by a simple object: **truncation error**.\n",
    " - Errors due to representing real numbers with a finite number of decimal places, and making operations with floating point numbers instead of making the true operations on the real numbers: **rounding error**.\n",
    "\n",
    "In this course, we will consider only the last two types of error, but it is important to remember that there are many possible sources of error:\n",
    " - It does not make sense to waste valuable resources searching for the exact solution of a mathematical problem which is only an approximation to the true problem of interest.\n",
    " - Our methods should be robust to errors.\n",
    "\n",
    "*Except for lab examples, we will never know the error exactly, but we will sometimes be able to bound it*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Model error\n",
    "\n",
    "These are usually intentional simplifications, whose effect in the solution can be estimated\n",
    " - We ignore forces known to act on the body, but known to have small magnitude.\n",
    " - We approximate a number of objects (thus an integer), by a real number.\n",
    " - We approximate a complex geometry by a simpler one.\n",
    " - We assume that fluid layers do not mix.\n",
    " - A large etcetera\n",
    "\n",
    "But sometimes model error is very hard to estimate:\n",
    " - The effect of clouds in climate models.\n",
    " - Bubbles and crisis are influenced by human perceptions.\n",
    " - Another large etcetera"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Truncation error\n",
    "\n",
    "Errors in substituting a mathematical object by a simpler one:\n",
    " - approximate a function by another: $\\sin(x) \\approx x$ for $|x| \\approx 0$.\n",
    " - approximate a number by another: $x$, solution of $10x + \\sin(x)=1$ can be approximated by the solution of $10x=1$\n",
    " - we will see many other examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Absolute and relative error\n",
    "\n",
    "If $x$ is the number we want, and $y$ is the approximation:\n",
    "- Absolute error, which is measured in the same units as $x$ and $y$:\n",
    "$$ e = |x-y|$$\n",
    "- Relative error which, being dimensionless, can be interpreted in any situation:\n",
    "$$\n",
    "    r = \\frac{e}{|x|} = \\frac{|x - y|}{|x|}\n",
    "$$\n",
    "Quadratic error (also called squared error) is also used often:\n",
    "$$ e = (x-y)^2$$\n",
    "although it may seem as a deformation of the real error, with the wrong units, it is often easier to work with quadratic error, and it has several other advantages.\n",
    "\n",
    "\n",
    "**Beware** think carefully which are the units for each type of error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Big-O notation\n",
    "\n",
    "Usually, an approximation method will have an associated __parameter__ that drives the tradeoff between less computing time and less error.\n",
    "It is usually chosen so that the error is below a certain _threshold_.\n",
    "In this situation, we will be interested in the effect of the parameter choice over the error. The relation between them will usually be complicated, but a rough approximation is enough, and this is the motivation for the **big-O notation**:\n",
    "\n",
    "$$    f(x) =  O(g(x)) \\quad \\text{when} \\quad x \\rightarrow a   $$ \n",
    "\n",
    "if and only if\n",
    "\n",
    "$$    |f(x)| \\leq M |g(x)| \\quad \\text{when}\\quad  |x - a| < \\delta, \\quad \\text{where} \\quad M,a,\\delta > 0. $$ \n",
    "\n",
    "In practice, we will use the big-O  notation to quantify the effect of approximating a power series when we discard all the terms starting with a given one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Big-O with $x\\rightarrow \\infty$\n",
    "\n",
    "In the above definition, $a$ can take the value $\\infty$, which means:\n",
    "\n",
    "$$    f(x) =  O(g(x)) \\quad \\text{when} \\quad x \\rightarrow \\infty  $$ \n",
    "\n",
    "if and only if\n",
    "\n",
    "$$    |f(x)| \\leq M |g(x)| \\quad \\text{when}\\quad  |x| > K, \\quad \\text{where} \\quad M,K > 0. $$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Properties of the big-O notation**\n",
    "\n",
    "Let us study two properties of the big-O notation when $x\\rightarrow\\infty$.\n",
    "\n",
    "Suppose:\n",
    "\n",
    "$$\\begin{aligned}\n",
    "    f(x) &= p(x) + O(x^n) \\\\\n",
    "    g(x) &= q(x) + O(x^m) \\\\\n",
    "    k &= \\max(n, m)\n",
    "\\end{aligned}$$\n",
    "Then\n",
    "$$\n",
    "    f+g = p + q + O(x^k)\n",
    "$$\n",
    "and\n",
    "\\begin{align}\n",
    "    f \\cdot g &= p \\cdot q + p O(x^m) + q O(x^n) + O(x^{n + m}) \\\\\n",
    "    &= p \\cdot q + O(x^{n+m})\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On the contrary, if we are interested in small values of x (this means $a=0$), let us write the formulas in terms of a small $\\Delta x$.\n",
    "\n",
    "If\n",
    "\n",
    "\\begin{align}\n",
    "    f(\\Delta x) &= p(\\Delta x) + O(\\Delta x^n) \\\\\n",
    "    g(\\Delta x) &= q(\\Delta x) + O(\\Delta x^m) \\\\\n",
    "    r &= \\min(n, m)\n",
    "\\end{align}\n",
    "then\n",
    "$$\n",
    "    f+g = p + q + O(\\Delta x^r)\n",
    "$$\n",
    "and\n",
    "\\begin{align}\n",
    "    f \\cdot g &= p \\cdot q + p \\cdot O(\\Delta x^m) + q \\cdot O(\\Delta x^n) + O(\\Delta x^{n+m}) \\\\\n",
    "    &= p \\cdot q + O(\\Delta x^r)\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Truncation error in Taylor's theorem\n",
    "\n",
    "**Taylor's theorem:**  Let $f(x) \\in C^{N+1}[a,b]$, $x_0 \\in [a,b]$.\n",
    "\n",
    "For any $x \\in (a,b)$ there is a number $c = c(x)$ in the interval between $x_0$ and $x$ such that\n",
    "\n",
    "$$ f(x) = T_N(x) + R_N(x)$$\n",
    "\n",
    "where $T_N(x)$ is the Taylor polinomial of $f$ of degree $N$ at $x0$:\n",
    "\n",
    "$$T_N(x) = \\sum^N_{n=0} \\frac{f^{(n)}(x_0)\\cdot(x-x_0)^n}{n!}$$\n",
    "\n",
    "and $R_N(x)$ is the Taylor error\n",
    "\n",
    "$$R_N(x) = \\frac{f^{(N+1)}(c) \\cdot (x - x_0)^{N+1}}{(N+1)!}$$\n",
    "\n",
    "This is a truncation error, independent of how many digits of precision we use to represent the real numbers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Bounding the truncation error\n",
    "\n",
    "In general **we cannot know the error precisely**. If we could, then we would know exactly the solution to our problem and we wouldn't need to compute it numerically.\n",
    "\n",
    "In general, we can only hope to:\n",
    "\n",
    " - **Bound the error.** Fund an upper bound for the error, typically as a function of some parameter that involves the amount of computational resources that we want to invest.\n",
    "\n",
    "For instance, the truncation error in the previous problem can be bounded if we can bound the derivative $f^{(N+1)}(c)$ by a constant $M$:\n",
    "\n",
    "$$\n",
    "    R_N(x) = \\frac{f^{(N+1)}(c) \\cdot (x-x_0)^{N+1}}{(N+1)!} \\leq \\frac{M (x-x_0)^{N+1}}{(N+1)!}\n",
    "$$\n",
    "\n",
    " - **Estimate the error.** Use an informed guess of what the error could be. In general this kind of estimation is used as a criterion inside a more complicated numerical method (adaptive quadrature, adaptive integration, etc)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Example of bounding the truncation error\n",
    "\n",
    "$f(x) = e^x$, $x_0 = 0$\n",
    "\n",
    "We can bound the error in the $T_2$ approximation, as a function of $x$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Derivatives:\n",
    "$$\\begin{aligned}\n",
    "    f'(x) &= e^x \\\\\n",
    "    f''(x) &= e^x \\\\ \n",
    "    f^{(n)}(x) &= e^x\n",
    "\\end{aligned}$$\n",
    "\n",
    "Taylor polynomials:\n",
    "$$\\begin{aligned}\n",
    "    T_N(x) &= \\sum^N_{n=0} e^0 \\frac{x^n}{n!} \\Rightarrow \\\\\n",
    "    T_2(x) &= 1 + x + \\frac{x^2}{2}\n",
    "\\end{aligned}$$\n",
    "\n",
    "Remainders (using $e<3$):\n",
    "$$\\begin{aligned}\n",
    "    R_N(x) &= e^c \\frac{x^{N+1}}{(N+1)!}\\Rightarrow \\\\\n",
    "    R_2(x) &= e^c \\cdot \\frac{x^3}{6} \\leq \\frac{e^x}{6}\n",
    "\\end{aligned}$$\n",
    "\n",
    "In other words, we approximate\n",
    "$e^1 = 2.718$ by $T_2(1) = 2.5$, and we know it makes an error smaller than $\\frac{e^1}{6}<0.5$.\n",
    "In fact, the error is smaller."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Symbolic computations with `sympy`\n",
    "\n",
    "We use [sympy](https://www.math.purdue.edu/~bradfor3/ProgrammingFundamentals/Sympy/) in order to compute Taylor polynomials symbolically. `sympy` is a library for performing __symbolic computations__, which means that it manipulates abstract objects that exactly represent numbers like π, π/2 o $\\sqrt{2}$, or functions like the cosine. The computations have all the precision that we may need, but are slower.\n",
    "\n",
    "The object of this course is however __numerical calculus__, which is most common in engineering, where numbers and functions are represented by aproximations which carry errors that we try to keep under control. On the plus side, it is _more efficient_."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import sympy\n",
    "x = sympy.symbols('x')\n",
    "f = sympy.symbols('f', cls=sympy.Function)\n",
    "\n",
    "f = sympy.exp(x)\n",
    "f.series(x0=0, n=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "x = numpy.linspace(-1, 1, 100)\n",
    "T_N = 1.0 + x + x**2 / 2.0\n",
    "R_N = numpy.exp(1) * x**3 / 6.0\n",
    "\n",
    "plt.plot(x, T_N, 'r', x, numpy.exp(x), 'k', x, R_N, 'b')\n",
    "plt.plot(0.0, 1.0, 'o', markersize=10)\n",
    "plt.grid(True)\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"$f(x)$, $T_N(x)$, $R_N(x)$\")\n",
    "plt.legend([\"$T_N(x)$\", \"$f(x)$\", \"$R_N(x)$\"], loc=2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Example of estimation of the truncation error. Richardson extrapolation.\n",
    "\n",
    "We consider the problem of computing the derivative $f'(x_0)$ of a function $f$ at a point $x_0$. \n",
    "\n",
    "Thus we approximate:\n",
    "$$\n",
    "f'(x_0)\\approx D_{x_0}(h):=\\frac{f(x_0+h) - f(x_0-h)}{2h}.\n",
    "$$\n",
    "Taylor's theorem tells us that:\n",
    "$$\n",
    "f(x_0\\pm h)=f(x_0)\\pm hf'(x_0) + h^2 \\frac{f''(x_0)}{2}\\pm h^3 \\frac{f'''(c_h^\\pm)}{6},\n",
    "$$\n",
    "where $c_h^\\pm$ is a number between $0$ and $\\pm h$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, the truncation error satisfies:\n",
    "$$\n",
    "D_{x_0}(h)-f'(x_0)= \\frac{f(x_0+h) - f(x_0-h)}{2h} - f'(x_0) = h^2\\frac{f'''(c_h^+)+f'''(c_h^-)}{12}.\n",
    "$$\n",
    "which means:\n",
    "$$\n",
    "D_{x_0}(h)-f'(x_0)= \\frac{f(x_0+h) - f(x_0-h)}{2h} - f'(x_0) = O(h^2).\n",
    "$$\n",
    "In many practical applications we don't jave an analytic formula for $f$, but only its values over a set of points.\n",
    "Thus, we cannot bound the error because we don't know $f'''$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We make the following trick: we replace $h$ by $h/2$ in the previous formula and get:\n",
    "$$\n",
    "D_{x_0}(h/2) -f'(x_0)=  h^2\\frac{f'''(\\tilde{c}_h^+)+f'''(\\tilde{c}_h^-)}{48}\n",
    "$$\n",
    "If $h$ is small and $f'''$ doesn't change abruptly close to $x_0$, we can guess that:\n",
    "$$ \n",
    "c_h^\\pm = \\tilde{c}_h^\\pm = x_0.\n",
    "$$\n",
    "and we get:\n",
    "$$\n",
    "D_{x_0}(h) -f'(x_0)= h^2\\frac{f'''(x_0)}{6},\\quad D_{x_0}(h/2) -f'(x_0)= h^2\\frac{f'''(x_0)}{24}.\n",
    "$$\n",
    "Solving for $f'(x_0)$:\n",
    "$$\n",
    "f'(x_0)=\\frac{4D_{x_0}(h/2)-D_{x_0}(h)}{3},\n",
    "$$\n",
    "and this allows the following estimation of the error:\n",
    "$$\n",
    "D_{x_0}(h/2) -f'(x_0) \\approx \\frac{D_{x_0}(h)-D_{x_0}(h/2)}{3}.\n",
    "$$\n",
    "We can compute this last quantity without an explicit expression for $f'''$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Counting operations\n",
    "\n",
    " - As we said, when we study a numerical method, we must find a trade between the error and the time spent in the computation.\n",
    "\n",
    "In order to estimate computation time, which is highly dependent on the particular hardware used, it is usual to count the number of required arithmetic operations. The relation with computing time is usually not linear in practice, since many other factors can affect the final computation time in unpredictable ways:\n",
    " - the amount of each type of memory.\n",
    " - the number of processors.\n",
    " - some processors have instructions that are specific to speeding up some numerical computations.\n",
    " - some compilers perform some optimizations when they detect certain patterns.\n",
    " - a large etcétera.\n",
    "\n",
    "However, it is still useful to count the number of operations, and the conclussions that were obtained with this kind of analysis decades ago are still useful."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rounding error\n",
    "\n",
    "We will study rounding error in detail in the next notebook\n",
    "\n",
    "```\n",
    "022_rounding_error.ipynb\n",
    "```\n"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "latex_envs": {
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 0
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
