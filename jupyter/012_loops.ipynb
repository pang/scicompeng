{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "be8f78b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import time as tm"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e196bce",
   "metadata": {},
   "source": [
    "# `for` and `while` loops\n",
    "\n",
    "The `python` language offers many options for tweaking `for` and `while` loops so that we can write almost any loop as either of them.\n",
    "\n",
    "However, we recommend instead to not use those tricks, and always try to use each construction for the task it was built:\n",
    "\n",
    " - `for` loops are used to run over an _iterable_. In almost every example in this course, that iterable will be a `range` object, a list, or an array, and we will know its size right from the beginning.\n",
    " - we will only use `while` loops for coding iterative methods, in which some code is run over and over, until some \"stop criterion\" is satified."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4fa06c9",
   "metadata": {},
   "source": [
    "### Example\n",
    "\n",
    "Add the squares of the elements of an array.\n",
    " - Run over the array, which is an iterable.\n",
    " - We know the size of the array.\n",
    " - => a `for` loop is most appropriate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "42f1e9be",
   "metadata": {},
   "outputs": [],
   "source": [
    "myarray = np.arange(20)-7\n",
    "N = len(myarray)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "92987016",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "790"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mysum = 0\n",
    "for x in myarray:\n",
    "    mysum += x**2\n",
    "mysum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "0d940dbc",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "790"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# variant: we run over the array indices instead of running over the array values\n",
    "# The previous option is usually best, but sometimes this is necessary :-/\n",
    "myarray = np.arange(20)-7\n",
    "mysum = 0\n",
    "for i in range(N):\n",
    "    mysum += myarray[i]**2\n",
    "mysum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "ceb9379e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "790"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# variant: list comprehension, a shorter syntax, often easier to understand,\n",
    "#          roughly equivalent to the others in terms of performance\n",
    "mysum = sum(x**2 for x in myarray)\n",
    "mysum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "d8a9a96d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "790"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "## INAPPROPRIATE, A WHILE LOOP\n",
    "i = 0\n",
    "mysum = 0\n",
    "while i<N:\n",
    "    mysum += myarray[i]**2\n",
    "    i += 1\n",
    "mysum"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af8e6b7b",
   "metadata": {},
   "source": [
    "### When in doubt, the `for` loop is usually a better solution.\n",
    " - It's only too easy to write an infinite `while` loop\n",
    " - If you didn't write it, a `while` loop is harder to understand"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ece58b5",
   "metadata": {},
   "source": [
    "## When to use a `while` loop?\n",
    "\n",
    "There are many legitimate use cases for `while` loops, but in this course there is only one:\n",
    "\n",
    " - An _iterative method_ that repeats some computations until some \"stop criterion\" is satisfied.\n",
    "\n",
    "### Example\n",
    "\n",
    "The babilonian method for computing square roots:\n",
    "\n",
    " - GOAL: Compute the square root of `a`\n",
    " - INITIALIZATION: Our initial approximation is not good, for instance `x=1`, `x=a`.\n",
    " - REPEAT: If `x` is an approximation, then `(1/2)*x + (1/2)*(a/x)` is a better approximation\n",
    " - UNTIL: Stop when the difference between `x**2` and `a` is smaller than our __tolerance__ `epsilon`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "4b0b97dc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.4142135623746899 2.0000000000045106 4.510614104447086e-12\n"
     ]
    }
   ],
   "source": [
    "# GOAL\n",
    "a = 2\n",
    "# INITIALIZATION\n",
    "x = a\n",
    "# TOLERANCE\n",
    "epsilon = 1e-6\n",
    "# while the STOP CRITERION is not satisfied\n",
    "while abs(x**2-a)>epsilon:\n",
    "    # REPEAT\n",
    "    x = (1/2)*x + (1/2)*(a/x)\n",
    "\n",
    "# The final error is smaller than the tolerance\n",
    "print(x, x**2, abs(x**2-a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b9e5e3b",
   "metadata": {},
   "source": [
    "we pack it as a function for computing square roots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "7afceade",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.4142135623746899 2.0000000929222947\n"
     ]
    }
   ],
   "source": [
    "def root(a, # GOAL: sqrt(a)\n",
    "         epsilon = 1e-6 # TOLERANCE\n",
    "        ):\n",
    "    # INITIALIZATION\n",
    "    x = a\n",
    "    # while the STOP CRITERION is not satisfied\n",
    "    while abs(x**2-a)>epsilon:\n",
    "        # REPEAT\n",
    "        x = (1/2)*x + (1/2)*(a/x)\n",
    "    return x\n",
    "\n",
    "# The final error is smaller than the tolerance\n",
    "print(root(2), root(4))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63f4105c",
   "metadata": {},
   "source": [
    "#### Exercise: find the number `x*` such that `f(x*) = x*`\n",
    "\n",
    "Define $f(x) = e^{-x}$.\n",
    "Observing the graphs of f(x) and the identity function `x->x` in the interval $[0,1]$, it is clear that there is a number $0<x^*<1$ such that `f(x*) = x*`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "6d650dfe",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.legend.Legend at 0x7f2f8a37ff40>"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXQAAAD4CAYAAAD8Zh1EAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjQuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8rg+JYAAAACXBIWXMAAAsTAAALEwEAmpwYAAAxH0lEQVR4nO3dd3RU1fr/8fdOpySEdJIQEhJa6BB670WK5SqKKCh2Ecst6tWrXtRrRUXwqqh8BSt2Q+9ceug1tBAISSCVlgBpM/v3x4n+IgIJZJKTmXlea2UtJnNm5jkEPmz22fs5SmuNEEII++didgFCCCFsQwJdCCEchAS6EEI4CAl0IYRwEBLoQgjhINzM+uCAgAAdGRlp1scLIYRd2rZtW47WOvByz5kW6JGRkWzdutWsjxdCCLuklEq50nMy5SKEEA5CAl0IIRyEBLoQQjgICXQhhHAQEuhCCOEgyg10pdQspVSWUmrvFZ5XSqn3lVJJSqndSqkOti9TCCFEeSoyQv8cGHqV54cBTUq/HgA+rHxZQgghrlW5ga61XgOcusoho4E52rAJ8FVKNbBVgZdKyspn6tKDFBRbquojhBCiahRdgGUvwJnjVfL2tphDDwNSyzxOK/3enyilHlBKbVVKbc3Ozr6uD1uxP5PpK5MY/v5athy72r8zQghRgxxdAx92g/XT4PDSKvmIar0oqrWeqbWO01rHBQZedudquR7sE82ceztTVGLl1o828q9f9pJXUGzjSoUQwkYKzkL8ZJg9EpQLTFgAne6rko+yRaCnAw3LPA4v/V6V6d00kCVP9OaeHpF8mZDC4HfXsGJ/ZlV+pBBCXLsDC+GDLrDjC+g+GR5aD5E9q+zjbBHo8cDdpatdugJntdYnbfC+V1XH040XR7bkp4e74+PlzsTZW3n06+1k5RVU9UcLIcTV5WfD9/fAt3dALT+4bwUMfhk8alfpx5bbnEsp9Q3QFwhQSqUBLwLuAFrrj4CFwHAgCbgA3FNVxV5O+4j6zHusJx//7wjTVyax9lA2/xzegtviGuLioqqzFCGEs9Madn8Hi5+Gwnzo9xz0eALcPKrl45VZN4mOi4vTtu62eCQ7n2d/2sPmo6foHOXHf25qTUxQXZt+hhBCXNbZNJj/pHHBM7wTjJoBQc1t/jFKqW1a67jLPedQO0WjA+vy7f1deeOW1hw4eY7h09by7rJDssRRCFF1rFbY8il80BWOrYMhr8G9S6okzMtjWj/0quLiohjTKYL+zYN5ZUEi01YcZt6uE7xyUyu6RweYXZ4QwpHkJMG8yZCyHqL6wMhp4BdlWjkONUIvK9Dbk2m3t2fOvZ0psWrGfpLAU9/tJDe/0OzShBD2zlIC696Dj3pAxl5jeuXuX00Nc3CwOfQrKSi2MH3lYWauSaa2hxvPDmsuF02FENcnYw/8OglO7oTmI2D42+BTZZvj/8Rp5tCvxMvdlb8Pac7Cyb1oFuzNMz/t4S8fbWD/yXNmlyaEsBclhbDyFZjZF86lw62zYcyX1Rrm5XGKEXpZWmt+2JbGa4sOcPZiMfd0j+SJQU2p6+lwlxOEELZyPAHiH4Ocg9D2DhjyH6jtZ0opTj9CL0spxa1xDVnxVB9uiwvn03VHGTB1NQt2n8Ssf9yEEDVUYT4sehpmDYHiC3Dnj3DTR6aFeXmcLtB/U7+OB6/d3IafHulOQF1PHv16O3fP2kxydr7ZpQkhaoIjK41mWgkfGb1XHtkITQaaXdVVOW2g/6ZDRH3iJ/XkpZGx7Ew9w5D31vDm4gNcKCoxuzQhhBkunoZfHoUvbgJXD7hnMdzwNnh6m11ZuZxuDv1qsvMKeX3RAX7cnkZoPS/+NSKWoa1CUEpWwwjhFPbPhwVPwfkc6PE49Hka3L3MruoPZA69ggK9PZl6W1u+f6gbPrXcefgrYxomKUumYYRwaPlZ8N14mHsn1A2C+1fCwBdrXJiXR0boV1BisfJVwnHeXnqQi0UW7u0ZxWP9Y/D2cje7NCGErWgNu76Fxc9A8UXo+7TR5ta15v49v9oIXQK9HDn5hby1+CDfbUsloK4nzw5rzo3twmRTkhD27sxxmPcEHFkBDbsYuz0Dm5pdVblkyqUSAup68sZf2vDLIz0I9a3FU9/t4paPNrAr9YzZpQkhrofVCps/gf92g+ObYNibxoVPOwjz8sgI/RpYrZoft6fxxuKD5J4v5NaO4fx9SHMCvT3NLk0IURE5h40NQsc3QvQAGPke+EaYXdU1udoIXbZHXgMXF2NT0tBWIUxfmcT/rT/Kwj0ZTB4Qw4TuUXi4yX94hKiRLMWwYTqsfh3ca8GNHxo7Ph1sBZuM0CshOTufVxfsZ8WBLCL9a/PcDbEMbBEkyxyFqElO7jKaaWXshhajjGZa3sFmV3XdZA69ijQOrMtnEzox+97OuLm6cP+crdz12WYOZEjTLyFMV1wAy/8NM/tBXgbc9gWM+cKuw7w8MkK3kWKLla8TjvPu8kOcu1jM7Z0jeGpQUwLqyvy6ENUuZaMxV557GNrdCUNehVr1za7KJmTZYjU6c6GIaSsO88XGFGq5u/Jo/xgmdI/Ey93V7NKEcHyFebBiirGKxbehcQeh6P5mV2VTEugmOFI6v77yQBbh9Wvx9NDmjGjTQObXhagqScuNdeVn06DLg9D/X+DpeDeJlzl0E0QH1mXWhE58ObELdT3deOybHdzy4Qa2pZw2uzQhHMuFU/Dzw/DlLcYKlnuXwLA3HDLMyyMj9GpgsWp+2JbK20sPkZ1XyA2tG/CPoc1o5F/H7NKEsG+Jv8KCv8GFXOj5JPT+u931X7lWsg7dZK4uijGdIhjRJpSZa5KZuSaZpYkZ3NU1ksf6x1C/jofZJQphX/IyYeFfYf88aNAWxv0IDdqYXZXpZIRugsxzBby77BDfbU2ljqcbk/rFMF4unApRPq1h59ew5FljWWK/Z6HbY+DqPGNTuShaQx3MyOP1RftZdTCbMN9aPDWoKTe2D8NVGn8J8WenU2De45C8CiK6w6jpEBBjdlXVTgK9httwJIfXFh5gT/pZWjTw4emhzejTNFBWxAgBYLUYyxBXTDG26g98CeImgotzrumQQLcDVqtm/p6TvLXkAKmnLtKtsT/PDGtO24a+ZpcmhHmyDxobhFITIGYQjHjXWF/uxCTQ7UhRiZWvE1KYvjKJ3PNFDG8dwl8HNyM60PmWYAknZimG9e/B/94Ejzow9A1oc5vDNdO6HhLodii/sIRP1iTz6dpkCkqs3BYXzuMDmhJSz7GXZAnBiR3w62OQuQda3gTD3oK6gWZXVWNIoNuxnPxCZqxM4quEFFyUYkL3SB7qEy1LHYXjKb5otLfdMB3qBMINU6HFCLOrqnEqHehKqaHANMAV+FRr/folz0cAswHf0mOe0VovvNp7SqBfm9RTF3h32SF+3plOXQ83HujdmHt6RlHX03mWawkHdmy9MVd+6gh0uBsGvQy1fM2uqkaqVKArpVyBQ8AgIA3YAtyhtU4sc8xMYIfW+kOlVCywUGsdebX3lUC/Pocy83h7yUGWJmbiX8eDh/tGM65rI1nDLuxTwTlY/hJs/Qx8G8Go96FxX7OrqtEq28ulM5CktU7WWhcB3wKjLzlGAz6lv64HnLjeYsXVNQ32ZubdcfzyaA9aNPDhlQX76fvWar5KSKGoxGp2eUJU3OFlxn09t86Cro/CIxslzCupIiP0vwBDtdb3lT6+C+iitZ5U5pgGwFKgPlAHGKi13naZ93oAeAAgIiKiY0pKiq3Ow2ltOJLD20sOsv34GRr61eLxAU25sV0obq7OuUZX2IELp2DxM7B7LgQ2h1EzoGEns6uyG9XRbfEO4HOtdTgwHPhCKfWn99Zaz9Rax2mt4wID5aq1LXSPDuDHh7vzfxM64ePlzt++38Xg99YQv+sEVqs5F7yFuCytYe9PMKMT7P0R+jwND66RMLehigR6OlB2JX946ffKmgh8B6C13gh4AQG2KFCUTylFv+ZBzJvUk4/GdcDdxYXJ3+xg2LS1LNpzUoJdmO/cSZg7Dn64x9gY9MD/oN8/wU3u6GVLFQn0LUATpVSUUsoDuB2Iv+SY48AAAKVUC4xAz7ZloaJ8Li6Koa0asOjxXrx/R3uKrVYe/mo7N0xfx5J9GZi1RFU4Ma1h+xz4oItxA4pBL8PE5RDSyuzKHFJFly0OB97DWJI4S2v9qlJqCrBVax1furLlE6AuxgXSf2itl17tPWWVS9WzWDXxu9KZtvwwx3Iv0DLUhycGNmVgiyDpEyOq3qmjMG8yHF0DjXoaK1j8o82uyu7JxiInV2Kx8svOE0xfeZiU3Au0CvNhcv8mDIoNlmAXtme1QMLHsPJlUK4weAp0mOC0zbRsTQJdAEaw/7wjnRmrkkjJvUBsAx8mD2jC4NhgXKRlr7CFrP3w6yRI3wpNhhjNtOqFmV2VQ5FAF3/w24h9xkpjKqZ5iDeTBzRhaMsQCXZxfUqKYN27sOYt8PKBYW9Cq1ukmVYVkEAXl1VisTJv9wmmr0wiOfs8TYLqMql/DCPahMpNNkTFpW8zmmll7TNCfNibUEcWuVUVCXRxVRarZsGek8xYeZhDmflEBdThkb7R3Ng+DHfZoCSupOgCrP4PbPwA6obAiHeg2TCzq3J4EuiiQqxWzdLEDN5fkUTiyXOE+dbiob7R3NoxXHrFiD86utZopnX6KHQYD4NfBq96ZlflFCTQxTXRWrPyQBYzViWx4/gZgrw9ub9XY8Z2iaCOdHd0bgVnYdkLsO1zqB9lLEWM6m12VU5FAl1cF601G4/kMn1lEhuTc/Gt7c493aMY370RvrWlH7vTObgY5j8J+RnQ9RHo9xx41Da7KqcjgS4qbVvKaT5cncTy/VnU8XBlbJcI7uvVmGAfuYOSwzufA4uehr0/QFCs0UwrvKPZVTktCXRhMwcyzvHh6iPM23UCNxcXbu4QxgO9G9NY7nnqeLQ2mmgt+ofRt7z336DnU+Am/zszkwS6sLnjuRf4eM0Rvt+WRrHFyrBWITzUJ5o24b5mlyZs4Ww6LPgrHFoEYR2NUXlwrNlVCSTQRRXKzivk8w1HmbMxhbyCErpH+/Ngn2h6NwmQtgL2yGqF7bONC5+WYuj/PHR9GFxklVNNIYEuqlxeQTHfbD7OZ+uOknmukBYNfHiwd2NuaNNA1rLbi9wjMO9xOLYWInsZK1j8GptdlbiEBLqoNkUlVn7Zmc4na5I5nJVPmG8t7ukRye2dI+SG1jWVpQQ2/RdWvQquHsaa8g7jZdt+DSWBLqqd1apZdTCLj9cks/noKby93BjbJYJ7ukcRUk9WxtQYmfuMZlontkOz4XDDVPAJNbsqcRUS6MJUO1PP8MnaZBbtOYmLUoxqG8p9vRoTG+pT/otF1SgphLVTjS8vXxj+JrS8WUbldkACXdQIqacuMGv9UeZuSeVCkYUeMf7c17MxfZoGSpfH6pS2FX59FLIPQJsxMPR1qO1ndlWigiTQRY1y9kIxX28+zuwNx8g4V0B0YB3u7RnFze3DqeUhqymqTNF5WPmqMV/uEwoj3oOmg82uSlwjCXRRIxVbrCzcc5JP1x5lT/pZfGu7M7ZzBHd3i5R5dltLXg3xk+FMCnS6Dwa8aPQtF3ZHAl3UaFprthw7zWfrklmamImrUtzQpgH39IiiXUNfs8uzbxfPwNLnYccX4BcNo6ZDZA+zqxKVcLVAl3VkwnRKKTpH+dE5yo/juReYvfEYc7ek8uvOE3SI8GVCjyiGtQqR9ezX6sACmP8UnM+GHk9A32fAvZbZVYkqJCN0USPlFRTzw7Y0Zm84xrHcCwT7eHJX10bc0TkC/7qeZpdXs+Vnw6K/w76fIbgVjJ4Boe3NrkrYiEy5CLtltWpWH8ri/9YfY+3hHDzcXBjZJpQJ3SNpHS43VPgDrWHP90ZnxKJ86PMPY2Tu6m52ZcKGZMpF2C0XF0X/5sH0bx5MUlYeszek8OP2NH7cnkaHCF/Gd49kWKsGeLg5+XTM2TSjV/nhpRDeyWimFdTc7KpENZMRurA7Zy8a0zFfbDSmYwK9PbmjU0PGdmnkfKtjrFbYNguWvQTaYqxe6Xy/NNNyYDLlIhyS1apZczibORtTWHUwCxelGBwbzF3dGtGtsb/jd3vMSTLu63l8AzTuCyOnQf1Is6sSVUymXIRDcnFR9G0WRN9mQRzPvcBXCSnM3ZrKor0ZxATVZVyXCG7uGI6Pl4PNIVtKYOMMWP0auHnC6A+g3Z2ybV/ICF04loJiC/N3n+SLTSnsSj1DLXdXRrcLZVzXRrQKc4CLqBl7jG37J3dB8xFGMy3vELOrEtVIplyEU9qTdpYvN6Xw6650CoqttG3oy51dIhjZJtT+WgyUFMKat2Ddu1DLD254G2JHm12VMIEEunBqZy8W89P2NL5KOE5SVj7eXm7c0iGcsV0iaBrsbXZ55TueAPGTIOcQtL0DhvxHmmk5MQl0ITBaDGw+eoqvEo6zeG8GRRYrcY3qM7ZLBMNbN8DLvYaN2gvzYeXLkPAx1AuHke9BzECzqxImq3SgK6WGAtMAV+BTrfXrlznmNuAlQAO7tNZjr/aeEujCTLn5hfy0PZ2vNx/naM55fLzcuLlDOHd0jqBZSA0YtSetgHlPwNlUo5nWwBfBswbUJUxXqUBXSrkCh4BBQBqwBbhDa51Y5pgmwHdAf631aaVUkNY662rvK4EuagKtNZuST/HN5v8/am8f4cvtnRoyok0odar7tnkXT8OS52Hnl+DfxGim1ahb9dYgarTKBno34CWt9ZDSx88CaK1fK3PMm8AhrfWnFS1KAl3UNKfOF/HT9jS+3ZJKUlY+dTxcGdUulDGdImgbXq/q17UnxsPCv8H5HOjxOPR5GtydbKOUKFdl16GHAallHqcBXS45pmnpB63HmJZ5SWu9+DKFPAA8ABAREVGBjxai+vjV8eC+Xo2Z2DOKbSmn+XZLKr/sOME3m1NpFuzNbZ0aclP7MPzqeNj2g/MyjSDfHw8hreHO76FBW9t+hnAKFRmh/wUYqrW+r/TxXUAXrfWkMsfMB4qB24BwYA3QWmt95krvKyN0YQ/yCoqJ33WC77aksivtLB6uLgyKDebWuHB6NQnEtTK3ztMadn0Di5+F4oulzbQel2Za4qoqO0JPBxqWeRxe+r2y0oAErXUxcFQpdQhogjHfLoTd8vZy584ujbizSyMOZJxj7pZUftmRzoI9Jwnx8eKWjmH8pWNDogLqXNsbnzluNNNKWg4NuxjNtAKbVs1JCKdRkRG6G8ZF0QEYQb4FGKu13lfmmKEYF0rHK6UCgB1AO6117pXeV0bowl4VllhYuT+LuVtTWXMoG6uGTpH1ubVjQ4a3aUDdq11ItVph62ew/CVjhD7wReh0P7g4ebdIUWG2WLY4HHgPY358ltb6VaXUFGCr1jpeGVeLpgJDAQvwqtb626u9pwS6cAQZZwv4eUc6329LJTn7PLXcXRnWKoRbOobTrbE/LmWnZHIOlzbT2gjRA4x15b5yLUlcG9lYJEQV01qz/fgZftyexrxdJ8grKCG0nhc3dQjjlrbBND48C1a/YdwCbuhrxo5PaaYlroMEuhDVqKDYwtLETH7clkZu0hZed5tJK5djpAQPwveWd6kX1LD8NxHiCqR9rhDVyMvdlVGxfozK/gSdOo0L7vWZ4vYss1Ja4zFtL/2aZ3JT+3D6NQ/E062GtRsQdk0CXQhbS9lozJXnHka1G0edIa/wLy9fbjl5jp+2p/PrzhMs2ZdJvVru3NCmATe1DyOuUX3HvyGHqHIy5SKErRTmwfJ/w5ZPoF4EjJoG0f3/dFiJxcq6pBx+3pHO0n2ZXCy2EF6/Fje2C+PG9qHEBEnPFnFlMocuRFVLWl7aTCsNujwE/Z8Hz7rlviy/sISl+zL4eUc665NysGpoGerDje3CGNk21PnukSrKJYEuRFW5cAqW/NPY8RnQ1NggFHFpZ4yKycorYN6uk/y6M53daWdRCrpE+TG6XRjDWoXgW9vGLQeEXZJAF8LWtIbEX40eLBdPQ88nofffjXt82kBydj7xu07w684THM05j7urok/TQEa2DWVQbDC1PeTyl7OSQBfClvIyYMFf4cB8o4nW6A+MplpVQGvN3vRzxO9KZ96uk2ScK6CWuysDWgQxqm0ofZrJShlnI4EuhC1oDTu/MqZYSgqh77PQbRK4Vs9o2WrVbDl2ivhdJ1i45ySnLxTj7enG4JYhjGjbgJ4xAbi7SgsBRyeBLkRlnU6BeY9D8iqI6G7ceCIgxrRyii1WNhzJZf6uEyzel0FeQQm+td0ZEmuEe7fG/rhJuDskCXQhrpfVAps/gRVTjK36g/4NHe+tUc20CkssrD2Uw/zdJ1iWmMn5Igv1a7sztFUIw1tLuDsaCXQhrkf2Qfh1EqRthphBMOJd8K3Z2/YLii3871A2C3afZPn+TC6UhvuQlqXhHu0v0zJ2TgJdiGthKYb178H/3gSPOjD0DWhzm9010yootrD6YDYL95xkxX5j5O5b253BscEMa92AHtEBeLhJuNsbCXQhKurEDmNUnrkXWt4Ew96CuoFmV1Vpv43cF+/NYHliJnmFJXh7uTGwRTBDW4XQp2kgXu6yWsYeSHMuIcpTfBFWvwYbZkCdQBjzFbQYYXZVNuPl7sqQliEMaRlCYYmFdYdzWLQ3g2WJmfy8I51a7q70ax7IkJYh9G8ehLeX3AbPHkmgC3FsHcRPhlNHoP1dMPgVqOVrdlVVxtPNlQEtghnQIphii5VNybks3pvBkn2ZLNyTgYerCz1i/BnaKoSBLYLxr2ubzVKi6smUi3BeBedg+YuwdRb4NoJR70PjvmZXZRqLVbP9+GmW7M1g8b4M0k5fxEVBXCM/BrcMZkjLEBr61Ta7TKcnc+hCXOrQUpj/BJw7AV0fNpppeVzjjZ4dmNaaxJPnWLIvk6X7MjiQkQdAiwY+DI4NZlBsMC1DfaTlrwkk0IX4zflcWPwM7PkOApsb2/bDL/t3Q5SRknueZYmZLN2XydaUU1g1hPnWYmCLIAbFhtClsZ8sh6wmEuhCaA17f4RF/4CCs9Drr8aXjZppOZPc/EJWHMhi6b5M1iVlU1BsxdvLjb7NghjYIoi+zYKoV0suqlYVCXTh3M6dMJppHVwIoe2NUXlwS7OrcggXiyysS8pheWImKw5kkpNfhJuLolOkHwNaBDGwRTCRATKVZUsS6MI5aQ3bZ8PSf4GlCPr9E7o+Wm3NtJyNxarZmXqGFfszWb4/k0OZ+QBEB9ZhYItg+jUPIq5RfWlDUEkS6ML5nDoK8ybD0TXQqKexgsU/2uyqnMrx3AusOJDJiv1ZJBzNpdii8fFyo0+zIPo3D6RP0yD86shNO66VBLpwHlYLJHwEK14GV3ejmVaHCTWqmZYzyi8sYd3hbFbsz2LVwWxy8gtxUdCuoS/9mxvz7rJqpmIk0IVzyEyE+McgfSs0HQo3vAP1wsyuSlzCatXsST/LigNZrD6Yxe60swAE+3jSt2kQ/ZoH0iMmQHarXoEEunBsJUWw7h1Y8zZ4+cCwN6HVLXbXTMtZZecVsvpgFqsOZrH2cA55BSW4uSg6NqpP32ZB9G0WSPMQbxm9l5JAF44rfZvRTCsrEVrfCkNfhzoBZlclrlOxxcr2lNOsPpTN6oPZ7D95DjBG732aGvPuPWMCqFfbeUfvEujC8RRdgFWvwqb/Qt0QGPEONBtmdlXCxjLPFfC/g9n871A2aw9nc66g5Pe59z5Ng+jdNIA24b64ujjP6F0CXTiWo2uMZlqnj0LHe4wLn171zK5KVLESi5WdqWdYc8gI+N3pZ9Ea6tVyp2dMAL2aBNC7aSChvrXMLrVKSaALx1BwFpa9ANs+h/pRxn09o3qZXZUwyanzRaxLymHNoWzWHMomK68QMNa992oSSK8mAXRt7E8dT8fadyCBLuzfwUUw/0nIz4Ruj0Lff4KHdP4TBq01h7PyjXA/nMPmo7kUFFtxc1F0iKhPzyYB9GwSQJuwena/sanSga6UGgpMA1yBT7XWr1/huFuAH4BOWuurprUEuqiQ8zmw6GnY+wMEtYTR0yGso9lViRquoNjC9pTTrDmcw7qkbPadOIfW4O3pRtdof3rGBNAjJoDowDp2t3qmUncsUkq5Ah8Ag4A0YItSKl5rnXjJcd7A40BC5UsWTk9r2POD0UyrMM8Ykfd8EtxkZ6Eon5e7K91jAugeEwA059T5IjYcyWF9Ug7rknJYlpgJGKtnesQE0CPaCPiQel7mFl5JFZlc6gwkaa2TAZRS3wKjgcRLjnsZeAP4u00rFM7nbDoseAoOLYawOBg9A4JamF2VsGN+dTwY0SaUEW1CAaMtwfojRrivPpjNT9vTAWgcUIfuMf50jzbm3+2tNUFFAj0MSC3zOA3oUvYApVQHoKHWeoFS6oqBrpR6AHgAICIi4tqrFY7NaoXtn8PSF0BbYMhr0OVBcJGbFwvbivCvTYR/BHd0jsBq1RzIyGPDkRw2HMnl5+3pfLnpOADNQ7zpFm0EfOcovxrfFrjSl3+VUi7AO8CE8o7VWs8EZoIxh17ZzxYOJPcIzHscjq2FqN4w8n3wizK7KuEEXFwUsaE+xIb6cF+vxhRbrOxJP8uGpBw2JufydcJx/m/9MVwUtAytR9fGfnSL9qdTpF+Na09QkUBPBxqWeRxe+r3feAOtgNWlFxdCgHil1KjyLowKgaXE2By06lVw9YCR06DDeNm2L0zj7upCh4j6dIioz6T+TSgssbDj+Bk2HsllU3Iuszek8Mnao7goaBVWj66N/ekS5UenKD98TA74cle5KKXcgEPAAIwg3wKM1Vrvu8Lxq4G/ySoXUa6MvRA/CU7sgGbD4Yap4BNqdlVCXNVvK2g2HT3FpuRcdh4/Q5HFiouC2FAfukQZAd85yg/f2rafg6/UKhetdYlSahKwBGPZ4iyt9T6l1BRgq9Y63rblCodXUghrpxpfXr7wl1nQ8mYZlQu78McVNKUBf/w0CclGwH+xKYXP1h0FjDn4zqXh3jnSjyCfql1FIxuLRPVK22o008reD23GGM20avuZXZUQNlNYYmFX6lkSknPZfOwU21JOc6HIAkCkf206Rfpxe+eGdGx0fX/uKzVCF8Imis7DytJmWj5hMPZ7aDrY7KqEsDlPN9ffR+VgdJDcd+IcW46eIuHoKZbtz6RHTAAdG9n+syXQRdVLXm000zqTAp3ugwEvGn3LhXAC7q4utGvoS7uGvtzfuzFWq8ZSRTMjEuii6lw8A0ufhx1fgF80TFgIkT3MrkoIU7m4KFyomutFEuiiahxYAPOfgvPZ0OMJ6PsMuDt2W1MhzCaBLmwrP8vov7LvZwhuBWO/hdD2ZlclhFOQQBe2oTXs/g4WP21cAO33PPR4XJppCVGNJNBF5Z1JNXqVJy2D8M5GM63AZmZXJYTTkUAX189qhW2zYNmLoK3GmvLOD0gzLSFMIoEurk9OEsybDCnroXFfowdL/UizqxLCqUmgi2tjKYGNM2D1a+DmCaNmQPtxsm1fiBpAAl1UXMYeY9v+yZ3QfITRTMs7xOyqhBClJNBF+YoLYM1bsP49qOUHt82B2NFmVyWEuIQEuri64wlGi9ucQ9B2LAx5VZppCVFDSaCLyyvMh5UvQ8LHUC8cxv0IMQPNrkoIcRUS6OLPjqw0bgd3JhU63w8DXgBPb7OrEkKUQwJd/H8XT8OS52DnV+DfBO5ZBI26mV2VEKKCJNCFITEeFv4NzudAz6egz9PgXrV3VxFC2JYEurPLyzSCfH88hLSGO7+HBm3NrkoIcR0k0J2V1rDrG1j8LBRfNObJu08GV3PvWi6EuH4S6M7ozHGY9wQcWQENu8Ko6RDY1OyqhBCVJIHuTKxW2PIpLH/J2Ko//G2ImwguLmZXJoSwAQl0Z5Fz2Ni2n7oJovsbzbR8I8yuSghhQxLojs5SDBveh9VvGLeAu/EjaHu7NNMSwgFJoDuyk7vg10eNplqxo40plrpBZlclhKgiEuiOqPgi/O8NWP8+1AmAMV9Ci5FmVyWEqGIS6I4mZaPRTCs3CdqNgyGvQK36ZlclhKgGEuiOojAPlv8btnxiXOy862fj4qcQwmlIoDuCw8uMdeXn0qHLw9D/efCsa3ZVQohqJoFuzy6cMnZ67v4WAprBxKXQsLPZVQkhTCKBbo+0hsRfYOHfjQ6Jvf4Gff5h3ONTCOG0KrRFUCk1VCl1UCmVpJR65jLPP6WUSlRK7VZKrVBKNbJ9qQKAvAyYOw6+nwA+YfDAahjwLwlzIUT5I3SllCvwATAISAO2KKXitdaJZQ7bAcRprS8opR4G3gTGVEXBTktr2PGl0a/cUgiDpkDXR8FV/pMlhDBUJA06A0la62QApdS3wGjg90DXWq8qc/wmYJwti3R6p48ZdxBKXg2NesDI9yEgxuyqhBA1TEUCPQxILfM4DehyleMnAosu94RS6gHgAYCICOkjUi6rBTbPhBVTQLnADVOh473STEsIcVk2/f+6UmocEAf0udzzWuuZwEyAuLg4bcvPdjhZByD+MUjbDDGDYOR7xs2ahRDiCioS6OlAwzKPw0u/9wdKqYHAc0AfrXWhbcpzQpZiWPcerHkTPOrCTTOhzW3STEsIUa6KBPoWoIlSKgojyG8HxpY9QCnVHvgYGKq1zrJ5lc7ixA6jxW3mXmh1Cwx9A+oGml2VEMJOlBvoWusSpdQkYAngCszSWu9TSk0Btmqt44G3gLrA98oYSR7XWo+qwrodS/FFWP0abJgOdYPh9m+g+XCzqxJC2JkKzaFrrRcCCy/53gtlfj3QxnU5j2PrjLnyU8nQYbyxHLGWr9lVCSHskCxiNkvBOVj+ImydBfUj4e54aHzZa8lCCFEhEuhmOLQE5j8JeSeh2yTo9xx41Da7KiGEnZNAr07nc2HxM7DnOwhsAbfNgfA4s6sSQjgICfTqoDXs+wkW/gMKzkKfZ6DXX8HNw+zKhBAORAK9qp07AQv+CgcXQmgHGD0DgluaXZUQwgFJoFcVrWH7bFj6L2Oz0OBXoOsj4OJqdmVCCAclgV4VTiVD/GQ4thYie8HIaeAfbXZVQtQoxcXFpKWlUVBQYHYpNZKXlxfh4eG4u7tX+DUS6LZktUDCR7DiZXB1N4K8w3jZti/EZaSlpeHt7U1kZCRK/o78gdaa3Nxc0tLSiIqKqvDrJNBtJTMR4idB+jZoOhRueAfqhZldlRA1VkFBgYT5FSil8Pf3Jzs7+5peJ4FeWSVFsO4dWPM2ePnALZ8ZfVjkD6kQ5ZIwv7Lr+b2RQK+M9G1GM62sRGh9q9FMq46/2VUJIZyUBPr1KLoAq16FTf+FuiFwx1xoNtTsqoQQTk5ufXOtjq6BD7vBxhnGBc9HN0mYC+GgHnzwQerUqcPKlSsv+/zChQtp3rw5zz333DW976pVq2jRogVPPvmkLcr8nYzQK6rgLCx7AbZ9Dn6NYfx8iOpldlVCOIR/z9tH4olzNn3P2FAfXhx5/Zv4XnnlFc6cOUNCQgK33norc+fOpU2bNn845pNPPuGdd95h+PBra3fdr18/du/eTWBgIG+++eY1LU28GhmhV8TBRfBBF9g+B7pPhofWS5gL4SC+/PJLOnfuTLt27XjwwQexWCzMnj2bffv28fXXX9OqVSvi4+O5//77SU1N/cNrz5w5Q1BQ0BXfe/To0cyZMweAjz/+mDvvvPP359zd3fH19eXcOdv9QyYj9Ks5nwOL/gF7f4SglnD71xDWweyqhHA4lRlJV8b+/fuZO3cu69evx93dnUceeYSvvvqK8ePHM378+N+Pa9KkCQkJCX96vcViweUqN22fOXMmPXr0ICoqiqlTp7Jp06Y/PO/i4oLFYrHZ+UigX47WsOcHI8wL86DvP6Hnk9JMSwgHs2LFCrZt20anTp0AuHjx4lVH3GWdP3+eI0eOEBoaesVjgoODmTJlCv369ePnn3/Gz8/vD8+HhYWxe/duBg60zT2CZMrlUmfT4Osx8NN9xnb9h9ZC36clzIVwQFprxo8fz86dO9m5cycHDx7kpZdeKvd1CQkJRERE0Lt3b0JCQn7//nPPPUe7du1o167d79/bs2cP/v7+nDhx4k/vM3nyZEaOHMmUKVNscTrGCZnx1bFjR12jWCxab/5U61fDtH4lROsNH2htKTG7KiEcVmJiotkl6H379umYmBidmZmptdY6NzdXHzt2rEKvLSgo0JGRkTopKemKxyQkJOi2bdvq9PR0HRMTo5OTk//wfLt27fSGDRuu+PrL/R5h3Mv5srkqI3SA3CMweyQseMqYI394A3STzohCOLrY2FheeeUVBg8eTJs2bRg0aBAnT56s0Gs9PT1p1KgRp0+fvuzzhYWF3H///cyaNYvQ0FCmTp3Kvffei5HJhtOnT9OkSRObnAs4+xy6pQQ2fQCr/gOunjBqOrS/S7btC+FExowZw5gxY67rtbVr1yYrK+uyz3l6erJr167fH48aNYpRo0b9/rioqIhz585Ru7btbj/pvCP0jD3w6QBjbXn0AHg0ATrcLWEuhKiwhx56iOeff/66NhbFxcUxYcIEmwa6843QSwqNRlrr3gEvX/jLLGh5swS5EOKaXTrqrqjfNhbZmnMFeuoWo8Vt9gFoMwaGvg61/cp/nRBC2AHnCPSi87DyFdj0IfiEwZ0/QJNBZlclhBA25fiBnrzauB3cmRSImwgDXzL6lgshhINx3EC/eAaWPg87vgC/aLhnETTqbnZVQghRZRwz0PfPhwV/hfPZ0OMJ6PsMuNcyuyohhKhSjhXo+Vmw8O+Q+AsEt4ax30Joe7OrEkKUZ9EzxlJiWwppDcNev+LTW7ZsYeLEiWzevBmLxULnzp2ZO3curVq1sm0d1cgxAl1r2D0XFj9jXADt/y/o8Ti42qbHsBDC8XTq1IlRo0bx/PPPc/HiRcaNG2fXYQ6OEOhnUmH+E5C0HBp2MXZ7BjYzuyohxLW4yki6Kr3wwgt06tQJLy8v3n//fVNqsKUK7RRVSg1VSh1USiUppZ65zPOeSqm5pc8nKKUibV7ppaxW2PwJ/LcrpGyEYW/CPYslzIUQFZabm0t+fj55eXkUFBSYXU6llRvoSilX4ANgGBAL3KGUir3ksInAaa11DPAu8IatC/2DnMPw+XBY+DcI7wSPbIQuD8JVGs0LIcSlHnzwQV5++WXuvPNOnn76abPLqbSKTLl0BpK01skASqlvgdFAYpljRgMvlf76B2CGUkrpsm3FbGX7F8YKFvdacOOH0PYO2bYvhLhmc+bMwd3dnbFjx2KxWOjevTsrV66kf//+Zpd23SoS6GFA2RvppQFdrnSM1rpEKXUW8Adyyh6klHoAeAAgIiLi+ir2j4GmQ2D42+AdfH3vIYRwenfffTd33303AK6urpe9xZy9qdaLolrrmcBMgLi4uOsbvTfqZnwJIYT4g4pMOqcDDcs8Di/93mWPUUq5AfWAXFsUKIQQomIqEuhbgCZKqSillAdwOxB/yTHxwG+3yP4LsLJK5s+FEA5FYuLKruf3ptxA11qXAJOAJcB+4Dut9T6l1BSl1G+NgD8D/JVSScBTwJ+WNgohRFleXl7k5uZKqF+G1prc3Fy8vLyu6XXKrN/MuLg4vXXrVlM+WwhhvuLiYtLS0hxi/XdV8PLyIjw8HHf3P+54V0pt01rHXe419r9TVAhhl9zd3YmKijK7DIciO3GEEMJBSKALIYSDkEAXQggHYdpFUaVUNpBynS8P4JJdqE5Aztk5yDk7h8qccyOtdeDlnjAt0CtDKbX1Sld5HZWcs3OQc3YOVXXOMuUihBAOQgJdCCEchL0G+kyzCzCBnLNzkHN2DlVyznY5hy6EEOLP7HWELoQQ4hIS6EII4SBqdKDXyJtTV7EKnPNTSqlEpdRupdQKpVQjM+q0pfLOucxxtyiltFLK7pe4VeSclVK3lf6s9ymlvq7uGm2tAn+2I5RSq5RSO0r/fA83o05bUUrNUkplKaX2XuF5pZR6v/T3Y7dSqkOlP1RrXSO/AFfgCNAY8AB2AbGXHPMI8FHpr28H5ppddzWccz+gdumvH3aGcy49zhtYA2wC4syuuxp+zk2AHUD90sdBZtddDec8E3i49NexwDGz667kOfcGOgB7r/D8cGARoICuQEJlP7Mmj9B/vzm11roI+O3m1GWNBmaX/voHYIBSdn3H6HLPWWu9Smt9ofThJow7SNmzivycAV4G3gAcoddqRc75fuADrfVpAK11VjXXaGsVOWcN+JT+uh5wohrrszmt9Rrg1FUOGQ3M0YZNgK9SqkFlPrMmB/rlbk4ddqVjtHEjjt9uTm2vKnLOZU3E+BfenpV7zqX/FW2otV5QnYVVoYr8nJsCTZVS65VSm5RSQ6utuqpRkXN+CRinlEoDFgKPVU9pprnWv+/lkn7odkopNQ6IA/qYXUtVUkq5AO8AE0wupbq5YUy79MX4X9gapVRrrfUZM4uqYncAn2utpyqlugFfKKVaaa2tZhdmL2ryCN0Zb05dkXNGKTUQeA4YpbUurKbaqkp55+wNtAJWK6WOYcw1xtv5hdGK/JzTgHitdbHW+ihwCCPg7VVFznki8B2A1noj4IXRxMpRVejv+7WoyYHujDenLveclVLtgY8xwtze51WhnHPWWp/VWgdorSO11pEY1w1Gaa3t+f6FFfmz/QvG6BylVADGFExyNdZoaxU55+PAAAClVAuMQM+u1iqrVzxwd+lql67AWa31yUq9o9lXgsu5SjwcY2RyBHiu9HtTMP5Cg/ED/x5IAjYDjc2uuRrOeTmQCews/Yo3u+aqPudLjl2Nna9yqeDPWWFMNSUCe4Dbza65Gs45FliPsQJmJzDY7Joreb7fACeBYoz/cU0EHgIeKvMz/qD092OPLf5cy9Z/IYRwEDV5ykUIIcQ1kEAXQggHIYEuhBAOQgJdCCEchAS6EEI4CAl0IYRwEBLoQgjhIP4fK+9PyZeDQvoAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# We will learn soon the code for generating graphics\n",
    "#  this is also in the python cheatsheet, it's the most basic kind of graph\n",
    "import matplotlib.pyplot as plt\n",
    "xs = np.linspace(0,1,100)\n",
    "ys = np.exp(-xs)\n",
    "plt.plot(xs, ys, label='e^{-x}')\n",
    "plt.plot(xs, xs, label='x')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3a27d44",
   "metadata": {},
   "source": [
    "In order to compute this point x*, we will use the \"fixed point method\" (which does not always work, as we will see...):\n",
    "\n",
    " - GOAL: Compute x* such that x* = exp(-x*)\n",
    " - IDEA: If we apply f many times, we get `x*=f(f(f(... x...)))`, which looks like `f(x*)=f(f(f(f(... x...))))`.\n",
    " - INITIALIZATION: Our initial approximation can be any number in the [0,1] interval.\n",
    " - REPEAT: If x is an approximation, then `f(x)=exp(-x)` is a better approximation.\n",
    " - UNTIL: Stop when the difference between `x` and `exp(-x)` is smaller than our tolerance epsilon.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "0b029e40",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x*= 1 ; error= -0.6321205588285577\n"
     ]
    }
   ],
   "source": [
    "def f(x):\n",
    "    return np.exp(-x)\n",
    "\n",
    "def solution(\n",
    "    f, # GOAL\n",
    "    x0 = 1, #INITIALIZATION\n",
    "    epsilon = 1e-6 # TOLERANCE,\n",
    "        ):\n",
    "    # INITIALIZATION\n",
    "    x = x0\n",
    "    # WRITE YOUR CODE HERE, INCLUDING THE STOP CRITERION AND THE ITERATION TO BE REPEATED\n",
    "    \n",
    "    return x\n",
    "\n",
    "# The final error must be smaller than the tolerance\n",
    "x_ast = solution(f)\n",
    "print('x*=',x_ast, '; error=', f(x_ast) - x_ast)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc19222a",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "804ab310",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50c757a3",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "e11dbf6e",
   "metadata": {},
   "source": [
    "## Run over an iterable, but we can stop before we reach the end: for ... break/return\n",
    "\n",
    "In some cases, _we have an iterable_, we may even know its length, but the loop may stop prematurely, so that _we don't know how many times the loop will be repeated_. In this case it is convenient to use a `for` loop, whose syntax is more clear, but use the `break` keyword if we want to exit the loop before it reaches the end of the iterable. A `return` instruction can also work if we are inside a function which has already found the desired result.\n",
    "\n",
    "### Example: decide if a number is prime\n",
    "\n",
    "We will decide if a number `n` is primer with the most naive technique: we will attempt to divide it by all the smaller numbers.\n",
    "\n",
    "Our first version is a `for` loop without `break`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "d8a7cf81",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 1721492"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "eb2c0728",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1721492 is composite\n"
     ]
    }
   ],
   "source": [
    "# We use the boolean `is_prime` that starts as `True`, but we will pass all the divisibility test\n",
    "#  by smaller numbers. If it passes all such tests, `is_prime` will remain `True`.\n",
    "#  If one or more of those tests fail, `is_prime` will switch to `False`.\n",
    "is_prime = True\n",
    "for i in range(2,n):\n",
    "    # if i divides n\n",
    "    if n%i==0:\n",
    "        is_prime = False\n",
    "print(n, 'is prime' if is_prime else 'is composite')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cca4ffd8",
   "metadata": {},
   "source": [
    "Same idea, but a `while` loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "385872b2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1721492 is composite\n"
     ]
    }
   ],
   "source": [
    "is_prime = True\n",
    "i = 2\n",
    "while i<n:\n",
    "    # if i divides n\n",
    "    if n%i==0:\n",
    "        is_prime = False\n",
    "    i += 1\n",
    "print(n, 'is prime' if is_prime else 'is composite')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b26fbec",
   "metadata": {},
   "source": [
    "This `while` loop is more efficient. As soon as we find a divisor, we step out of the loop. We are not interested in the whole list of divisors, we just want to know if it is prime..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "70b92d34",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1721492 is composite\n"
     ]
    }
   ],
   "source": [
    "i = 2\n",
    "while (i<n) and (n%i==0):\n",
    "    i += 1\n",
    "if i==n:\n",
    "    is_prime = True\n",
    "else:\n",
    "    is_prime = False\n",
    "print(n, 'is prime' if is_prime else 'is composite')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec07e7a2",
   "metadata": {},
   "source": [
    "Comparamos la velocidad"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "afb60e55",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "versión lenta:  0.23559045791625977\n"
     ]
    }
   ],
   "source": [
    "before = tm.time()\n",
    "# Tu código aquí\n",
    "is_prime = True\n",
    "i = 2\n",
    "while i<n:\n",
    "    # if i divides n\n",
    "    if n%i==0:\n",
    "        is_prime = False\n",
    "    i += 1\n",
    "\n",
    "after = tm.time()\n",
    "print('versión lenta: ', after - before)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "d9b541a0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1721492 is composite\n",
      "versión alternativa:  0.0017707347869873047\n"
     ]
    }
   ],
   "source": [
    "before = tm.time()\n",
    "# Tu código aquí\n",
    "i = 2\n",
    "while (i<n) and (n%i==0):\n",
    "    i += 1\n",
    "if i==n:\n",
    "    is_prime = True\n",
    "else:\n",
    "    is_prime = False\n",
    "print(n, 'is prime' if is_prime else 'is composite')\n",
    "\n",
    "after = tm.time()\n",
    "print('versión alternativa: ', after - before)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f827061",
   "metadata": {},
   "source": [
    "### Exercise\n",
    "\n",
    " - Achieve the same result with a `for` loop and a `break` keyword."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0de9b19c",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "7f1c5d6b",
   "metadata": {},
   "source": [
    "### Exercise\n",
    "\n",
    " - Write code to compute the factorial of a number `n` using a loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9766ab95",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e55d97ff",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6312d5ea",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91c64b39",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
