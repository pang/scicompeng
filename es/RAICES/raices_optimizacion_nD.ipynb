{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ampliación de Matemáticas. Aproximación de raíces de funciones de varias variables. \n",
    "\n",
    "Por Pablo Angulo y Fabricio Macià para ETSIN@UPM."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Aproximación de raíces de funciones de varias variables y optimización "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.optimize import minimize_scalar, bisect, newton, root"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Resumen: raíces y puntos fijos en una variable\n",
    "\n",
    " - Si f(a) y f(b) tienen signos opuestos $\\Rightarrow$ f tiene una raíz en el intervalo [a,b]\n",
    "     - Método de bisección, que produce cotas del error cometido.\n",
    " - Si $g:[a,b]\\rightarrow [a,b]$ $\\Rightarrow$ g tiene un punto fijo en el intervalo [a,b]\n",
    " - Si $g:[a,b]\\rightarrow [a,b]$ **y** $|g'(x)|\\leq k < 1$, $\\Rightarrow$\n",
    "     * $g$ tiene un único punto fijo $x^*$ en el intervalo [a,b]\n",
    "     * la sucesión $x_{n+1}=g(x_n)$ converge a $x^*$, *para cualquier punto inicial* $x_0$.\n",
    "     * $|x_n - x^*| < \\frac{k^n}{1-k}|x_1-x_0|$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vamos a investigar la implementación del método de bisección en la librería `scipy.optimize`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return 1 + (x**3 - 4*x) + np.log(1+x**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "bisect(f, -1, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Qué pasa cuando no hay cambio de signo\n",
    "bisect(f, -1, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bisect?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bisect(f, -1, 1, full_output=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como veis, usando la opción `full_output=True` obtenemos información adicional: número de iteraciones, número de evaluaciones, y el \"flag\" que indica si ha convergido."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x0 = bisect(f,-1,1)\n",
    "\n",
    "xs = np.linspace(-2,2,100)\n",
    "ys = f(xs)\n",
    "plt.figure(figsize=(9,6))\n",
    "plt.plot(xs,ys)\n",
    "plt.axhline(color='k')\n",
    "plt.plot([-1,1], [0,0], 'o')\n",
    "plt.plot([x0], [0], 'o')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Resumen: métodos de Newton y Secante en una variable\n",
    "\n",
    " - El método de **Newton** busca raíces de f(x), iterando \n",
    "$$x_{n+1}=x_{n} - \\frac{f(x_n)}{f'(x_{n})}$$\n",
    "    - Es una iteración de punto fijo, con $g(x) = x - f(x)/f'(x)$.\n",
    "    - No es fácil saber si convergerá... pero cuando lo hace es de orden 2\n",
    "    - Hay que evaluar la derivada\n",
    " - El método de la **secante** es similar al de Newton, salvo porque:\n",
    "$$x_{n+1}=x_{n} - \\frac{f(x_n)\\left(x_{n}-x_{n-1}\\right)}{f(x_{n})-f(x_{n-1})}$$\n",
    "    - No hay que evaluar la derivada\n",
    "    - El orden de convergencia es algo menor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observamos ahora el método de Newton de `scipy.optimize`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newton(f, -1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Si el punto de partida no es bueno, no encuentra la solución\n",
    "newton(f, -1.28)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ejercicio\n",
    "\n",
    "El método `newton` no nos ha pedido la derivada de f: lee la documentación para averiguar qué método está usando realmente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newton?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "También podemos usar `full_output=True`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newton(f, -1, full_output=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para usar el método de Newton necesitamos calcular la derivada: lo podemos hacer a mano o de forma simbólica:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy as sym\n",
    "\n",
    "# Definimos un símbolo, que usaremos para definir funciones\n",
    "x = sym.symbols('x')\n",
    "\n",
    "# y es una función simbólica de x\n",
    "y = 1 + (x**3 - 4*x) + sym.log(1+x**2)\n",
    "\n",
    "# derivada es una función simbólica de x\n",
    "derivada = sym.diff(y,x)\n",
    "\n",
    "# lambdify convierte la función simbólica en una función \n",
    "# normal, que además acepta arrays de numpy como argumentos\n",
    "fp = sym.lambdify(x, derivada)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newton(f, -1, fprime=fp, full_output=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparamos con el método de la secante. El número de evaluaciones de f es similar, aunque varían dependiendo de la calidad de las aproximaciones iniciales."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newton(f, -1, x1=-1.2 ,full_output=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newton(f,x0=1,x1=1.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Curiosidad: Mostramos en qué raíz termina el método de Newton (cuando termina), dependiendo del punto de inicio."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r1 = bisect(f, -3, -1)\n",
    "r2 = bisect(f, -1,  1)\n",
    "r3 = bisect(f,  1,  3)\n",
    "cuenca_r1 = []\n",
    "cuenca_r2 = []\n",
    "cuenca_r3 = []\n",
    "fallo = []\n",
    "xtol = 1e-8\n",
    "for x in np.linspace(-3,3,500):\n",
    "    try:\n",
    "        r = newton(f, x, fprime=fp, tol=xtol)\n",
    "        if np.abs(r-r1)<xtol:\n",
    "            cuenca_r1.append(x)\n",
    "        elif np.abs(r-r2)<xtol:\n",
    "            cuenca_r2.append(x)\n",
    "        elif np.abs(r-r3)<xtol:\n",
    "            cuenca_r3.append(x)\n",
    "        else:\n",
    "            fallo.append(x)\n",
    "    except RuntimeError:\n",
    "        fallo.append(x)\n",
    "\n",
    "plt.figure(figsize=(8,8))\n",
    "for r,cuenca in [(r1, cuenca_r1), (r2, cuenca_r2), (r3, cuenca_r3)]:\n",
    "    xs = np.array(cuenca)\n",
    "    ys = f(xs)\n",
    "    plt.plot(xs, ys, '.', label='cuenca de %.3f'%r)\n",
    "    \n",
    "xs = np.array(fallo)\n",
    "ys = f(xs)    \n",
    "plt.plot(xs, ys, '.', label='el método falló')\n",
    "plt.axhline(color='k')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Métodos híbridos\n",
    "\n",
    "Combinando métodos como los anteriores, se obtienen métodos prácticos como el de **Brent**.\n",
    "\n",
    "### Ejercicio:\n",
    "\n",
    "Lee la [documentación oficial de scipy](https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html#scalar-functions) sobre optimización de funciones escalares: busca el método recomendado.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Raíces y puntos fijos en varias variables\n",
    "\n",
    "> - Si f(a) y f(b) tienen signos opuestos $\\Rightarrow$ f tiene una raíz en el intervalo [a,b]\n",
    "\n",
    "Tiene un equivalente teórico en varias variables, pero no tiene aplicación práctica general.\n",
    "\n",
    "> - Si $g:[a,b]\\rightarrow [a,b]$ $\\Rightarrow$ g tiene un punto fijo en el intervalo [a,b]\n",
    "\n",
    "En varias variables, **teorema del punto fijo de Brouwer**:\n",
    "\n",
    "- **Toda aplicación continua** $g:\\mathbf{B}\\rightarrow\\mathbf{B}$, donde $\\mathbf{B}\\subset\\mathbb{R}^n$ es un conjunto \"sin agujeros\" (por ejemplo, si es **convexo**), **tiene al menos un punto fijo**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "> - Si $g:[a,b]\\rightarrow [a,b]$ **y** $|g'(x)|\\leq k < 1$, $\\Rightarrow$\n",
    ">     * $g$ tiene un único punto fijo $x^*$ en  [a,b]\n",
    ">     * $x_{n+1}=g(x_n)$ converge a $x^*$, *para cualquier punto inicial* $x_0$.\n",
    ">     * $|x_n - x^*| < \\frac{k^n}{1-k}|x_1-x_0|$\n",
    "\n",
    "Cierto, casi sin cambios, en varias variables:\n",
    "\n",
    " - Si $g:\\mathbf{B}\\rightarrow \\mathbf{B}$ **y** $\\Vert D g(x)\\Vert\\leq k < 1$, $\\Rightarrow$\n",
    "     * $g$ tiene un único punto fijo $x^*$ en $\\mathbf{B}$\n",
    "     * la sucesión $x_{n+1}=g(x_n)$ converge a $x^*$, *para cualquier punto inicial* $x_0$.\n",
    "     * $|x_n - x^*| < \\frac{k^n}{1-k}|x_1-x_0|$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Si $g:\\mathbb{R}^n\\rightarrow\\mathbb{R}^n$, tenemos, por el teorema fundamental del cálculo, para $y=x+h$:\n",
    "\n",
    "$$ g(y)-g(x)=\\left(\\int _{0}^{1}Dg(x+th)\\cdot h\\,dt\\right)$$\n",
    "\n",
    "luego si $\\Vert D g(x) \\Vert \\leq k$ para todo $x$, tenemos que\n",
    "\n",
    "$$\n",
    "\\begin{split}\n",
    "\\Vert g(y) - g(x)\\Vert\n",
    "&=\\left\\Vert\\int _{0}^{1}Dg(x+th)\\cdot h\\,dt\\right\\Vert\\\\\n",
    "&\\leq \\int _{0}^{1} \\Vert Dg(x+th)\\cdot h\\Vert\\,dt\\\\\n",
    "&\\leq k\\int _{0}^{1} \\Vert h\\Vert\\,dt\n",
    "\\leq k \\Vert y-x\\Vert \n",
    "\\end{split}\n",
    "$$\n",
    "\n",
    "Por tanto, si $\\Vert D g(x)\\Vert\\leq k < 1$, *aplicar g reduce distancias*.\n",
    "\n",
    "**Nota**: No hemos hablado aún de las *normas matriciales*, $\\Vert D g(x) \\Vert$, pero veremos más adelante que para una norma adecuada, $\\Vert D g(x) \\Vert \\leq k\\Rightarrow \\Vert D g(x)\\cdot v \\Vert \\leq k\\Vert v\\Vert$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Una aplicación tal que $\\Vert g(y)-g(x)\\Vert \\leq k\\Vert y- x\\Vert$, para $k<1$, se llama **contractiva**, y la imagen de un conjunto se reduce hasta que converge a un punto.\n",
    "\n",
    "![contractive mapping](contractive_mapping.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "Definimos la sucesión $x_{n+1} = g(x_n)$, comenzando con un punto cualquiera $x_0$:\n",
    "\n",
    "$$\\Vert x_{n+1} - x_n\\Vert = \\Vert g(x_n) - g(x_{n-1})\\Vert \\leq k \\Vert x_n-x_{n-1}\\Vert $$\n",
    "\n",
    "y del mismo modo que en una variable:\n",
    "\n",
    "$$\\Vert x_n - x^*\\Vert < k^n\\Vert x_0-x^\\ast\\Vert$$\n",
    "\n",
    "o tb:\n",
    "\n",
    "$$\\Vert x_n - x^*\\Vert < \\frac{k^n}{1-k}\\Vert x_1-x_0\\Vert$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Relación entre optimización y minimización\n",
    "\n",
    "Los problemas de encontrar raíces y de minimizar (o maximizar) funciones son parecidos:\n",
    "$$f:\\mathbb{R}^n\\rightarrow\\mathbb{R}^n$$\n",
    "$$h:\\mathbb{R}^n\\rightarrow\\mathbb{R}$$\n",
    "\n",
    " - Los mínimos de $h$ son **puntos críticos** $\\Rightarrow$ son __raíces__ del sistema $\\nabla h = \\mathbf{0}$.\n",
    " - Si $h$ es **convexa**, el único mínimo de $h$ es la única raíz de $\\nabla h = \\mathbf{0}$.\n",
    " - Una función $f:\\mathbb{R}^n\\rightarrow\\mathbb{R}^n$ puede tener **muchas raíces** en un intervalo; una función $h:\\mathbb{R}^n\\rightarrow\\mathbb{R}$ puede tener **muchos mínimos y máximos locales**.\n",
    " - $f(\\mathbf{x_0})=\\mathbf{0}$ $\\Rightarrow$ La función $x\\rightarrow\\Vert f(\\mathbf{x})\\Vert^2$ alcanza un mínimo absoluto en $\\mathbf{x_0}$.\n",
    " - Hay un *análogo directo al método de bisección* para minimizar funciones de **una** variable: [Golden-ratio search](https://en.wikipedia.org/wiki/Golden-section_search)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "> En funciones convexas, minimizar f y encontrar raíces de f' es equivalente (incluso \"si f no es diferenciable\").\n",
    "\n",
    "Sin embargo, hay una *diferencia* importante:\n",
    "\n",
    " - **Todas las ráices de una función son equivalentes** a priori: nos basta con una, o bien las queremos todas.\n",
    " - Cuando buscamos el mínimo de una función, nos interesa sobre todo el **mínimo absoluto**. A menudo es preferible un punto x con valor pequeño de f a un mínimo local con mayor valor de f (f(y)>f(x))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Teorema de Weierstrass\n",
    "\n",
    "> Una función continua siempre alcanza su valor máximo y un mínimo en un conjunto \"cerrado\" y acotado.\n",
    "\n",
    "También conocido como [Teorema de los valores extremos](https://en.wikipedia.org/wiki/Extreme_value_theorem)\n",
    "\n",
    "Problemas:\n",
    "- Una función definida en un conjunto no acotado puede no tener un mínimo ni un mínimo.\n",
    "- El máximo o el mínimo puede estar en el interior, o en la frontera del conjunto...\n",
    "- Una función puede tener muchos máximos y mínimos locales"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Mínimos locales y globales\n",
    "\n",
    "Si una función tiene muchos mínimos locales, un algoritmo de búsqueda de mínimos puede _\"quedarse atascado\"_ en un mínimo local, y no alcanzar el mínimo absoluto..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return (x - 2) * x * (x + 2)* (x+3)\n",
    "\n",
    "xs = np.linspace(-3.5,2.5,100)\n",
    "ys = f(xs)\n",
    "res1 = minimize_scalar(f, bounds = (-3,0), method='bounded')\n",
    "res2 = minimize_scalar(f, bounds = (0,2), method='bounded')\n",
    "minima = np.array([res1.x, res2.x])\n",
    "\n",
    "plt.figure(figsize=(9,6))\n",
    "plt.plot(xs,ys)\n",
    "plt.plot(minima, f(minima), 'og')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "#Using the Bounded method, we find a local minimum with specified bounds as:\n",
    "res = minimize_scalar(f, bounds=(-3, -1), method='bounded')\n",
    "res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "minimize_scalar(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "En el ejemplo anterior, una llamada a `minimize_scalar` sin argumentos devolvía el mínimo absoluto, pero no tiene por qué ser así:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return (x - 2) * x * (x + 2)* (x+3)*(1+(x-1)**2)\n",
    "\n",
    "# Using the Brent method, we find the local minimum as:\n",
    "res1 = minimize_scalar(f)\n",
    "print(res1.x, res1.fun)\n",
    "#Using the Bounded method, we find a local minimum with specified bounds as:\n",
    "res2 = minimize_scalar(f, bounds=(-3, -1), method='bounded')\n",
    "print(res2.x, res2.fun)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "xs = np.linspace(-3.5,2.5,100)\n",
    "ys = f(xs)\n",
    "fig, ax = plt.subplots(figsize=(9,5))\n",
    "ax.plot(xs,ys)\n",
    "ax.plot([res1.x, res2.x], [res1.fun, res2.fun], 'go')\n",
    "ax.set_ybound(-50,50)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Método de Newton en varias variables\n",
    "\n",
    "> - El método de **Newton** busca raíces de f(x), iterando \n",
    "> $$x_{n+1}=x_{n} - \\frac{f(x_n)}{f'(x_{n})}$$\n",
    "\n",
    "- El método de **Newton** en varias variables usa esta iteración:\n",
    "$$x_{n+1}=x_{n} - \\left(\\nabla f(x_n)\\right)^{-1}f(x_n)$$\n",
    "\n",
    "    - Es una iteración de punto fijo, con $g(x) = x - \\left(\\nabla f(x_n)\\right)^{-1}f(x)$.\n",
    "    - No es fácil saber si convergerá... pero cuando lo hace es de orden 2\n",
    "    - Hay que evaluar la matriz jacobiana, *pero no es necesario invertirla*, basta con resolver el sistema $\\left(\\nabla f(x_n)\\right)y_n = f(x_n)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Más métodos numéricos para encontrar raíces\n",
    "\n",
    "\n",
    "Para encontrar raíces de funciones $f:\\mathbb{R}^n\\rightarrow\\mathbb{R}^n$ hay muchos métodos ([matlab](https://es.mathworks.com/help/optim/unconstrained-optimization.html)):\n",
    "\n",
    "> ``scipy.optimize.root`` ([scipy.optimize](https://docs.scipy.org/doc/scipy/reference/optimize.html))\n",
    "\n",
    "```\n",
    "    method : str, optional\n",
    "    Type of solver. Should be one of\n",
    "\n",
    "            ‘hybr’ ‘lm’ ‘broyden1’ ‘broyden2’ ‘anderson’ ‘linearmixing’ ‘diagbroyden’ ‘excitingmixing’ ‘krylov’ ‘df-sane’\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Para encontrar raíces de funciones $f:\\mathbb{R}^n\\rightarrow\\mathbb{R}^n$ hay muchos métodos:\n",
    "\n",
    " - Ninguno ofrece garantías completas de encontrar la raíz.\n",
    " - Algunos de hecho intentan minimizar $\\Vert f(x)\\Vert^2$.\n",
    " - Los de tipo **\"Broyden\"**, son similares al de Newton, pero intentan evitar calcular el gradiente (en el espíritu del método de la **secante**).\n",
    " - **Ejercicio: Escoged un método, aprended a usarlo y guardadlo para mañana**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Optimización de funciones de variables variables\n",
    "\n",
    " - Sabemos que es imposible encontrar el valor óptimo de forma garantizada, *excepto si f es convexa*.\n",
    " - La optimización puede terminar en un mínimo o máximo local.\n",
    " \n",
    "Algunas ideas importantes:\n",
    "\n",
    "#### **Descenso del gradiente** y sus variantes:\n",
    "\n",
    "> Para acercarte a un mínimo de $f$, avanza un poco en la dirección $-\\nabla f(x)$\n",
    "\n",
    " - Los algoritmos de esta familia tardan poco en alcanzar un **mínimo local**.\n",
    " - El mínimo local puede tener un valor muy superior al mínimo global."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Simmulated annealing\n",
    "\n",
    "(el objetivo es maximizar la función)\n",
    "- Una temperatura alta acepta soluciones inferiores al \"mejor valor encontrado hasta el momento\".\n",
    "- Una temperatura baja **no** acepta soluciones inferiores al \"mejor valor encontrado hasta el momento\".\n",
    "- Comienza con una temperatura alta, y la baja progresivamente...\n",
    "\n",
    "![Hill_Climbing_with_Simulated_Annealing](Hill_Climbing_with_Simulated_Annealing.gif)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Basin hopping\n",
    "\n",
    "- Un salto aleatorio\n",
    "- Después sigue el gradiente hasta un mínimo local\n",
    "- Acepta o rechaza el cambio en función de la *temperatura*\n",
    "\n",
    "#### Genetic algorithms\n",
    "\n",
    "Combina varias ideas de inspiración biológica:\n",
    "\n",
    "- Almacena varios puntos $x_i$, en lugar de sólo uno\n",
    "- Mutaciones\n",
    "- Recombinaciones\n",
    "- Selección natural"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### ... y muchos más\n",
    "\n",
    "- [matlab](https://es.mathworks.com/help/optim/unconstrained-optimization.html)\n",
    "- [scipy.optimize](https://docs.scipy.org/doc/scipy/reference/optimize.html)\n",
    "> ``scipy.optimize.minimize`` \n",
    "\n",
    "```   'Nelder-Mead' 'Powell' 'CG' 'BFGS' 'Newton-CG' 'L-BFGS-B' 'TNC' 'COBYLA' 'SLSQP' 'trust-constr' 'dogleg' 'trust-ncg' 'trust-krylov' 'trust-exact'\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Ejercicios\n",
    "\n",
    " - **Probad varios métodos para encontrar raíces de funciones en varias variables.**\n",
    " - **Probad varios métodos para minimizar funciones en varias variables.**\n",
    "> **¡No es necesario que lo programéis!**\n",
    " - **Responded a estas preguntas**:\n",
    "     - ¿por qué las librerías de software ofrecen métodos de minimización pero no de maximización?\n",
    "     - ¿qué métodos implementan las funciones que has elegido? ¿necesitas conocer el gradiente de la función?\n",
    "     - ¿hay alguna circunstancia en la que puedas asegurar que los métodos que has elegido funcionarán?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ejercicio\n",
    "\n",
    "Queremos determinar el flujo de agua a través de una tubería.\n",
    "\n",
    "Encuentra el factor de fricción de Darcy que resuelve la [ecuación de Colebrook - White](https://en.wikipedia.org/wiki/Darcy_friction_factor_formulae#Colebrook%E2%80%93White_equation)\n",
    "\n",
    "$${\\displaystyle {\\frac {1}{\\sqrt {f}}}=-2\\log \\left({\\frac {\\varepsilon }{3.7D_{\\mathrm {h} }}}+{\\frac {2.51}{\\mathrm {Re} {\\sqrt {f}}}}\\right)} $$\n",
    "\n",
    "con los siguientes datos:\n",
    "\n",
    " - Diámetro $D_h$: `8e-2`\n",
    " - Número de Reynolds: `2e5`\n",
    " - Rugosidad $\\varepsilon$: `2e-4`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Ejercicio\n",
    "\n",
    "Encuentra una solución, si la hay, del sistema de ecuaciones:\n",
    "$$\n",
    "\\begin{array}{rr}\n",
    "x^2 - y^2 &= 2\\\\\n",
    "xy&=2\n",
    "\\end{array}\n",
    "$$\n",
    "Prueba al menos un par de métodos distintos. ¿Encuentras siempre la misma solución?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Ejercicio\n",
    "\n",
    "Busca un mínimo global de la función de Rosenbrock de dos variables:\n",
    "\n",
    "$$f(x,y)=(a-x)^{2}+b(y-x^{2})^{2}$$\n",
    "\n",
    "para a=1, b=100, usando al menos dos métodos distintos."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.optimize\n",
    "scipy.optimize.minimize?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Ejercicio\n",
    "\n",
    "Consideramos la aplicación $F(x,y) = \\left(\\frac{y}{2}, \\frac{-x}{2} + 10 \\right)$.\n",
    "El profesor te garantiza que es contractiva con constante $k=\\frac{1}{2}$ (es la composición de una rotación, una homotecia de factor 1/2 y un desplazamiento).\n",
    "\n",
    "- Calcula las iteraciones sucesivas de esta función partiendo de distintos puntos iniciales.\n",
    "- Dibuja la evolución del error cometido como función del número de iteraciones.\n",
    "- Compara las estimaciones del error con las vistas más arriba."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Ejercicio\n",
    "\n",
    "Repite el ejercicio anterior para\n",
    "$$F(x,y) = \\left(\\cos\\left(\\frac{y}{2}\\right)+5, \\frac{x+y}{3} + 1 \\right)$$\n",
    "\n",
    "- Calcula las iteraciones sucesivas de esta función partiendo de distintos puntos iniciales.\n",
    "- Dibuja la evolución del error cometido como función del número de iteraciones.\n",
    "- Compara las estimaciones del error con las vistas más arriba.\n",
    "- ¿Crees que es contractiva?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
