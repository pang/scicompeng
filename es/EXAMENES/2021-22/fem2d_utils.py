import numpy as np
from scipy.spatial import Delaunay

def square_mesh(n, L=1):
    nboundary_vertices = 4*n
    ninterior_vertices = (n-1)*(n-1)
    
    nvertices = (n+1)*(n+1)
    vertices = np.zeros((nvertices, 2))
    xx, yy = np.meshgrid(np.linspace(0,L,n+1), np.linspace(0,L,n+1))
    vertices[:,0] = xx.reshape(nvertices)
    vertices[:,1] = yy.reshape(nvertices)
    
    ivertices_boundary = (
        (vertices[:,0]==0) | #south
        (vertices[:,1]==1) | #east
        (vertices[:,0]==1) | #north
        (vertices[:,1]==0)   #west
    )
    ivertices_boundary = [
        iv for iv in range(nvertices) if
        ((vertices[iv][0]==0) | #south
         (vertices[iv][1]==1) | #east
         (vertices[iv][0]==1) | #north
         (vertices[iv][1]==0)   #west
        )
    ]
    triangles = np.zeros((2*n*n,3), dtype=np.int32)
    itriangle_counter = 0
    for i in range(n):
        for j in range(n):
            triangles[itriangle_counter,:] = [i*(n+1)+j, i*(n+1)+j+1, (i+1)*(n+1)+j]
            triangles[itriangle_counter+1,:] = [i*(n+1)+j+1, (i+1)*(n+1)+j+1, (i+1)*(n+1)+j]
            itriangle_counter += 2

    return vertices, ivertices_boundary, triangles

def refine_mesh(vertices, ivertices_boundary, triangles):
    nvertices, _ = vertices.shape
    nvertices_boundary = len(ivertices_boundary)
    nvertices_interior = nvertices - nvertices_boundary
    vertices_boundary = vertices[ivertices_boundary]
    vertices_interior = vertices[[j for j in range(nvertices)
                                  if j not in ivertices_boundary]]
    ntriangles, _ = triangles.shape
    new_nvertices_boundary = 2*nvertices_boundary
    new_ivertices_boundary = np.arange(new_nvertices_boundary)
    new_nvertices_interior = nvertices_interior + ntriangles
    
    new_vertices = np.zeros((new_nvertices_boundary + new_nvertices_interior, 2))
    new_vertices[:new_nvertices_boundary:2, :] = vertices_boundary
    for j in range(nvertices_boundary):
        iv  = ivertices_boundary[j]
        iv1 = ivertices_boundary[(j+1)%nvertices_boundary]
        new_vertices[2*j+1, :] = 0.5*(vertices[iv]+vertices[iv1])
    new_vertices[new_nvertices_boundary:new_nvertices_boundary+nvertices_interior, :] = vertices_interior
    for j in range(len(triangles)):
        triangle = triangles[j]
        v1,v2,v3 = triangle
        vbarycenter = new_nvertices_boundary+nvertices_interior+j
        barycenter = vertices[triangle].mean(axis=0)
        new_vertices[vbarycenter, :] = barycenter
    
    tri = Delaunay(new_vertices)
    new_triangles = tri.simplices
    return new_vertices, new_ivertices_boundary, new_triangles
