{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " - Contenido Creative Commons Attribution license, CC-BY \n",
    " - Código con licencia MIT\n",
    " \n",
    "![creative commons attribution](./images/CC-BY.png)\n",
    "\n",
    " - (c) Kyle T. Mandli\n",
    " - (c) Modificado por Pablo Angulo y Fabricio Macià para ETSIN@UPM"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Método numérico\n",
    "\n",
    "Un método numérico es un procedimiento para aproximar la solución de un problema matemático:\n",
    "  - No todos los problemas admiten soluciones exactas.\n",
    "  - El cálculo simbólico tampoco puede resolver todos los problemas.\n",
    "  - Incluso aunque la solución exista, puede ser demasiado costoso encontrar esa solución.\n",
    "  - Muchos métodos de cálculo exacto o simbólico no tienen garantías de terminar en tiempo razonable.\n",
    "\n",
    "En general, un método numérico debe ser capaz de aproximar la solución al problema matemático con tanta precisión como queramos.\n",
    "En la práctica, tanta precisión es innecesaria, y muy costosa.\n",
    "Todos los métodos numéricos deben aproximar la solución buscando un equilibrio entre:\n",
    "  - poco error\n",
    "  - poco tiempo de cómputo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Tipos de error\n",
    "\n",
    "- Errores en los datos: **error de medición**, __ruido__.\n",
    "- Errores al simplificar la realidad mediante un modelo matemático: **error de modelo**.\n",
    "- Errores al reemplazar el objeto matemático de interés (número, vector, función, etc) por otro objeto matemático más fácil de calcular: **error de truncamiento**.\n",
    "- Errores al representar números reales con una cantidad finita de cifras decimales, y al hacer operaciones con números de coma flotante en vez de con números reales: **error de redondeo**.\n",
    "\n",
    "En esta asignatura, hablaremos únicamente de los dos últimos tipos de error, pero es importante recordar que hay muchas fuentes de error:\n",
    "  - No tiene sentido buscar una solución del problema matemático con precisión mayor que la permiten los errores de medición o de modelo.\n",
    "  - Es interesante diseñar métodos robustos a errores.\n",
    "  \n",
    "*Salvo en ejemplos de laboratorio, nunca podremos conocer exactamente el error, pero a veces podremos acotarlo.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Error de modelo\n",
    "\n",
    "A menudo son simplificaciones conscientes, cuyo efecto en la solución se puede estimar\n",
    " - Ignoramos fuerzas que sabemos que actúan pero pensamos que tienen poca magnitud\n",
    " - Aproximamos el número de barcos por un número real\n",
    " - Simplificamos la geometría del buque\n",
    " - Asumimos que no hay mezcla entre capas de fluído\n",
    " - un largo etcétera\n",
    "\n",
    "Pero también a veces el error de modelo es muy difícil de estimar\n",
    " - Efecto de las nubes en modelos climáticos\n",
    " - Burbujas y crisis son influenciados por percepciones humanas\n",
    " - otro largo etcétera"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Error de truncamiento\n",
    "\n",
    "Errores al sustituir un objeto matemático por otro más sencillo:\n",
    " - aproxima una función por otra: $\\sin(x) \\approx x$ for $|x| \\approx 0$.\n",
    " - aproxima un número por otro: $x$, solución de $10x + \\sin(x)=1$ se puede aproximar por la solución de $10x=1$\n",
    " - veremos otros muchos ejemplos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Error absoluto y relativo\n",
    "\n",
    "Si $x$ es el número que buscamos, $y$ es la aproximación:\n",
    "- Error absoluto:\n",
    "$$ e = |x-y|$$\n",
    "- Error relativo:\n",
    "$$\n",
    "    r = \\frac{e}{|x|} = \\frac{|x - y|}{|x|}\n",
    "$$\n",
    "También se usa a menudo el error cuadrático, o error al cuadrado:\n",
    "$$ e = (x-y)^2$$\n",
    "aunque a priori es una deformación del error real, y no tiene las unidades correctas, a menudo es más fácil de trabajar con este tipo de error.\n",
    "\n",
    "\n",
    "**Atención** a las unidades de cada tipo de error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### La notación O-grande\n",
    "\n",
    "En muchas situaciones, una aproximación tendrá un parámetro asociado a alla. Normalmente, el valor del parámetro se elige de forma que el error tiene un tamaño razonable en el contexto que estemos manejando. En tal caso, nos interesará conocer el impacto que el hacer variar el valor del parámetro tiene sobre el error. Esta es la motivación de la notación O-grande:\n",
    "\n",
    "$$    f(x) =  O(g(x)) \\quad \\text{cuando} \\quad x \\rightarrow a   $$ \n",
    "\n",
    "si y sólo si\n",
    "\n",
    "$$    |f(x)| \\leq M |g(x)| \\quad \\text{cuando}\\quad  |x - a| < \\delta \\quad \\text{donde} \\quad M,a > 0. $$ \n",
    "\n",
    "En la práctica, utilizaremos la notación O-grande para cuantificar el efecto que tiene aproximar una serie de potencias descartando todos los términos a partir de uno dado."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Notación O-grande cuando $x\\rightarrow \\infty$\n",
    "\n",
    "En la definición anterior, se puede estudiar el caso en que $x\\rightarrow\\infty$ en vez de $x\\rightarrow a$ para $a\\in\\mathbb{R}$, lo que quiere decir:\n",
    "\n",
    "$$    f(x) =  O(g(x)) \\quad \\text{cuando} \\quad x \\rightarrow \\infty  $$ \n",
    "\n",
    "si y sólo si\n",
    "\n",
    "$$    |f(x)| \\leq M |g(x)| \\quad \\text{cuando}\\quad  |x| > K, \\quad \\text{donde} \\quad M,K > 0. $$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "**Cómo se comporta la notación O-grande. Propiedades:**\n",
    "\n",
    "Hay dos resultados que nos permiten entender como se comporta una O-grande de x cuando el valor de la variable x **es muy grande** (cuadno $x \\rightarrow \\infty$): \n",
    "\n",
    "Sea\n",
    "$$\\begin{aligned}\n",
    "    f(x) &= p(x) + O(x^n) \\\\\n",
    "    g(x) &= q(x) + O(x^m) \\\\\n",
    "    k &= \\max(n, m)\n",
    "\\end{aligned}$$\n",
    "entonces\n",
    "$$\n",
    "    f+g = p + q + O(x^k)\n",
    "$$\n",
    "y\n",
    "\\begin{align}\n",
    "    f \\cdot g &= p \\cdot q + p O(x^m) + q O(x^n) + O(x^{n + m}) \\\\\n",
    "    &= p \\cdot q + O(x^{n+m})\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Por el contrario, si lo que nos interesa son los **valores pequeños** de x (es decir, $x \\rightarrow 0$), pongamos que se trata de un incremento $\\Delta x$ pequeño, las fórmulas correspondientes son: \n",
    "\n",
    "\\begin{align}\n",
    "    f(\\Delta x) &= p(\\Delta x) + O(\\Delta x^n) \\\\\n",
    "    g(\\Delta x) &= q(\\Delta x) + O(\\Delta x^m) \\\\\n",
    "    r &= \\min(n, m)\n",
    "\\end{align}\n",
    "entonces\n",
    "$$\n",
    "    f+g = p + q + O(\\Delta x^r)\n",
    "$$\n",
    "y\n",
    "\\begin{align}\n",
    "    f \\cdot g &= p \\cdot q + p \\cdot O(\\Delta x^m) + q \\cdot O(\\Delta x^n) + O(\\Delta x^{n+m}) \\\\\n",
    "    &= p \\cdot q + O(\\Delta x^r)\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Error de truncamiento en el Teorema de Taylor\n",
    "\n",
    "**Teorema de Taylor:**  Sea $f(x) \\in C^{N+1}[a,b]$, $x_0 \\in [a,b]$.\n",
    "\n",
    "Para cualquier $x \\in (a,b)$ existe un número $c = c(x)$ en el intervalo entre $x_0$ y $x$ tal que\n",
    "\n",
    "$$ f(x) = T_N(x) + R_N(x)$$\n",
    "\n",
    "donde $T_N(x)$ es el polinomio de Taylor de $f$ de grado $N$ en $x0$:\n",
    "\n",
    "$$T_N(x) = \\sum^N_{n=0} \\frac{f^{(n)}(x_0)\\cdot(x-x_0)^n}{n!}$$\n",
    "\n",
    "y $R_N(x)$ es el error de Taylor\n",
    "\n",
    "$$R_N(x) = \\frac{f^{(N+1)}(c) \\cdot (x - x_0)^{N+1}}{(N+1)!}$$\n",
    "\n",
    "Es un error de truncamiento, independiente de cuántos dígitos de precisión usemos para representar los números reales."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Acotar el error de truncamiento\n",
    "\n",
    "En general **no podemos conocer exactamente el error**, puesto que si fuera el caso, conoceríamos exactamente la solución a nuestro problema y no sería necesario calcularlo numéricamente.\n",
    "\n",
    "En general, sólo podemos esperar:\n",
    "\n",
    "- **Acotar el error.** Obtener una cota superior para el error, típicamente en función de algún parámetro que involucre la cantidad de recursos computacionales que queramos invertir.\n",
    "\n",
    "Por ejemplo, el error de truncamiento en el problema anterior se puede acotar si podemos acotar la derivada $f^{(N+1)}(c)$ por una constante $M$:\n",
    "\n",
    "$$\n",
    "    R_N(x) = \\frac{f^{(N+1)}(c) \\cdot (x-x_0)^{N+1}}{(N+1)!} \\leq \\frac{M (x-x_0)^{N+1}}{(N+1)!}\n",
    "$$\n",
    "\n",
    "- **Estimar el error.** Obtener una suposición fundamentada de cual puede ser el error. En general este tipo de estimación se utiliza como criterio dentro de un método numérico más complicado (cuadratura adaptativa, integración adaptativa, etc.). No es útil como indicador *per se*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Ejemplo de acotación del error de truncamiento\n",
    "\n",
    "$f(x) = e^x$, $x_0 = 0$\n",
    "\n",
    "Podemos acotar el error que comete la aproximación $T_2$, en función de $x$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Derivadas:\n",
    "$$\\begin{aligned}\n",
    "    f'(x) &= e^x \\\\\n",
    "    f''(x) &= e^x \\\\ \n",
    "    f^{(n)}(x) &= e^x\n",
    "\\end{aligned}$$\n",
    "\n",
    "Polinomios de Taylor:\n",
    "$$\\begin{aligned}\n",
    "    T_N(x) &= \\sum^N_{n=0} e^0 \\frac{x^n}{n!} \\Rightarrow \\\\\n",
    "    T_2(x) &= 1 + x + \\frac{x^2}{2}\n",
    "\\end{aligned}$$\n",
    "\n",
    "Restos (usando $e<3$):\n",
    "$$\\begin{aligned}\n",
    "    R_N(x) &= e^c \\frac{x^{N+1}}{(N+1)!}\\Rightarrow \\\\\n",
    "    R_2(x) &= e^c \\cdot \\frac{x^3}{6} \\leq \\frac{e^x}{6}\n",
    "\\end{aligned}$$\n",
    "\n",
    "Es decir, aproximamos \n",
    "$e^1 = 2.718$ por $T_2(1) = 2.5$, y sabemos que comete un error menor que $\\frac{e^1}{6}<0.5$.\n",
    "En realidad, el error es menor."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Cálculo simbólico con `sympy`\n",
    "\n",
    "Usamos [sympy](https://www.math.purdue.edu/~bradfor3/ProgrammingFundamentals/Sympy/) para calcular polinomios de Taylor simbólicamente. `sympy` es una librería para hacer __cálculo simbólico__, en la que se hacen manipulaciones con objetos abstractos que representan números como π, π/2 o $\\sqrt{2}$, o funciones como el coseno, de forma exacta. Los cálculos tienen toda la precisión que queramos, pero son más lentos.\n",
    "\n",
    "El objeto de esta asignatura es el __cálculo numérico__, el más usado en ingeniería, donde las operaciones arrastran errores, pero que intentamos mantener bajo control. A cambio, el __cálculo numérico__ es más eficiente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import sympy\n",
    "x = sympy.symbols('x')\n",
    "f = sympy.symbols('f', cls=sympy.Function)\n",
    "\n",
    "f = sympy.exp(x)\n",
    "f.series(x0=0, n=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "x = numpy.linspace(-1, 1, 100)\n",
    "T_N = 1.0 + x + x**2 / 2.0\n",
    "R_N = numpy.exp(1) * x**3 / 6.0\n",
    "\n",
    "plt.plot(x, T_N, 'r', x, numpy.exp(x), 'k', x, R_N, 'b')\n",
    "plt.plot(0.0, 1.0, 'o', markersize=10)\n",
    "plt.grid(True)\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"$f(x)$, $T_N(x)$, $R_N(x)$\")\n",
    "plt.legend([\"$T_N(x)$\", \"$f(x)$\", \"$R_N(x)$\"], loc=2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Ejemplo de estimación del error de truncamiento. Extrapolación de Richardson\n",
    "\n",
    "Consideremos el problema de calcular la derivada $f'(x_0)$ de una función $f$ en un punto $x_0$. \n",
    "\n",
    "Para ello aproximamos:\n",
    "$$\n",
    "f'(x_0)\\approx D_{x_0}(h):=\\frac{f(x_0+h) - f(x_0-h)}{2h}.\n",
    "$$\n",
    "El teorema de Taylor nos dice que:\n",
    "$$\n",
    "f(x_0\\pm h)=f(x_0)\\pm hf'(x_0) + h^2 \\frac{f''(x_0)}{2}\\pm h^3 \\frac{f'''(c_h^\\pm)}{6},\n",
    "$$\n",
    "donde $c_h^\\pm$ es un número entre $0$ y $\\pm h$.\n",
    "Por tanto, el error de truncamiento satisface:\n",
    "$$\n",
    "D_{x_0}(h)-f'(x_0)= \\frac{f(x_0+h) - f(x_0-h)}{2h} - f'(x_0) = h^2\\frac{f'''(c_h^+)+f'''(c_h^-)}{12}.\n",
    "$$\n",
    "Es decir:\n",
    "$$\n",
    "D_{x_0}(h)-f'(x_0)= \\frac{f(x_0+h) - f(x_0-h)}{2h} - f'(x_0) = O(h^2).\n",
    "$$\n",
    "En muchas aplicaciones prácticas no conocemos una fórmula analítica para $f$, sólo tenemos un conjunto de datos. **No podemos acotar el error** porque no conocemos $f'''$. \n",
    "\n",
    "Hacemos el truco siguiente: reemplazamos $h$ por $h/2$ en la fórmula anterior. Obtenemos:\n",
    "$$\n",
    "D_{x_0}(h/2) -f'(x_0)=  h^2\\frac{f'''(\\tilde{c}_h^+)+f'''(\\tilde{c}_h^-)}{48}\n",
    "$$\n",
    "Si $h$ es pequeño y $f'''$ no varía brúscamente cerca de $x_0$ podemos suponer que:\n",
    "$$ \n",
    "c_h^\\pm = \\tilde{c}_h^\\pm = x_0.\n",
    "$$\n",
    "Queda entonces:\n",
    "$$\n",
    "D_{x_0}(h) -f'(x_0)= h^2\\frac{f'''(x_0)}{6},\\quad D_{x_0}(h/2) -f'(x_0)= h^2\\frac{f'''(x_0)}{24}.\n",
    "$$\n",
    "Despejando obtenemos:\n",
    "$$\n",
    "f'(x_0)=\\frac{4D_{x_0}(h/2)-D_{x_0}(h)}{3},\n",
    "$$\n",
    "lo cual nos permite estimar el error:\n",
    "$$\n",
    "D_{x_0}(h/2) -f'(x_0) \\approx \\frac{D_{x_0}(h)-D_{x_0}(h/2)}{3}.\n",
    "$$\n",
    "Esta última cantidad la podemos calcular sin necesidad de conocer explícitamente la función $f'''$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Contar operaciones\n",
    "\n",
    " - Como hemos dicho, al elegir un método numérico hay que estudiar el error que comete y el tiempo de cómputo que necesita.\n",
    "\n",
    "Para estimar el tiempo de cómputo, es habitual contar el número de operaciones necesarias. La relación con el tiempo de cómputo no es lineal, ya que a menudo otros factores afectan al tiempo de cómputo de formas difíciles de prever:\n",
    " - la cantidad de memoria de cada tipo (cache o RAM)\n",
    " - el número de procesadores\n",
    " - las instrucciones específicas de algunos procesadores para cálculo numérico\n",
    " - las optimizaciones que hacen algunos compiladores cuando detectan ciertos patrones\n",
    " - un largo etcétera\n",
    "\n",
    "En todo caso, sigue siendo útil contar el número de operaciones."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Error de redondeo\n",
    "\n",
    "Lo analizaremos en detalle en el siguiente cuaderno\n",
    "\n",
    "```\n",
    "errores_redondeo.ipynb\n",
    "```\n"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "latex_envs": {
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 0
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
