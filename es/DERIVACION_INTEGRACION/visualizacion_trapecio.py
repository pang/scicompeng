import numpy as np
import scipy.integrate as integ
import matplotlib.pyplot as plt

def TA_verboso(f, a, b, tol, puntos=None, fa=None, fb=None):
    '''Regla del trapecio adaptativa
    
    I, E, puntos = TA_verboso(f, a, b, tol, fa=None, fb=None)
    
    INPUTS:
    - f: integrando
    - a,b: extremos del intervalo de integración
    - tol: tolerancia para la regla de parada. El algoritmo se detiene
           cuando la estimación del error es menor que la tolerancia.
    
    OUTPUTS:
    - I: aproximación de la integral
    - E: estimación del error
    - puntos: puntos donde se ha evaluado la función
           
    Los parámetros opcionales sólo se usan en las llamadas recursivas
    - puntos es un conjunto de puntos al que se añaden todos los puntos
      donde se evalúa f
    - fa, fb: parámetros opcionales que se usan para no volver a evaluar f
              en puntos donde ya ha sido evaluada
    '''
    #En la primera llamada a esta función, no se pasa el argumento "puntos"
    #En las siguientes llamadas, que son llamadas recursivas, "puntos" es
    #un conjunto con todos los puntos donde hemos evaluado la función
    if puntos is None:
        puntos = set([a,b])
    if fa is None:
        fa = f(a)
    if fb is None:
        fb = f(b)
    m = (a + b)/2
    h = (b - a)
    fm = f(m)
    I1 = h*(fa + fb)/2
    I2 = (h/2)*(fa + 2*fm + fb)/2
    E = (1/3)*np.abs(I1 - I2)
    puntos.add(m)
    if E<tol:
        return I2, E, puntos
    else:
        I_am, error_am, _ = TA_verboso(f, a, m, tol/2, puntos, fa=fa, fb=fm)
        I_mb, error_mb, _ = TA_verboso(f, m, b, tol/2, puntos, fa=fm, fb=fb)
        return (I_am + I_mb, 
                error_am + error_mb, 
                puntos)

def muestra_visualizacion_trapecio(f, a, b, tols):
    i_quad, e = integ.quad(f,a,b)
    print('integral de la función usando scipy quad:', i_quad)
    print('estimación del error de scipy quad:', e)
    for tol in tols:
        i_adap, estimacion_error_adap, puntos = TA_verboso(f, a, b, tol)
        error_adap = np.abs(i_adap-i_quad)
        lpuntos = np.array(sorted(puntos))
        print('-'*10)
        print('tolerancia:',tol)
        print('aproximación a la integral:',i_adap)
        print('estimación del error:', estimacion_error_adap)
        print('error:', error_adap)
        print('número de iteraciones:', len(puntos))
        plt.figure(figsize=(9,6))
        xs = np.linspace(a,b,200)
        ys = f(xs)
        plt.plot(xs, ys, 'g-', label='integral de f: %.5f'%i_quad)
        for punto in lpuntos:
            plt.axvline(x=punto, color='b')
        plt.plot(lpuntos, f(lpuntos), 'k', label='trapecio: %.5f'%i_adap)
        plt.title('Error: %.5f usando %d evaluaciones'%(error_adap, len(puntos)))
        plt.legend()
        plt.show()