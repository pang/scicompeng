\documentclass[12pt, a4paper, leqno]{amsart}
%\RequirePackage[...]{amsmath2000}
%\usepackage[...]{amsthm2000}



\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amscd}

\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[a4paper]{geometry}
\usepackage{hyperref}
\usepackage{color}
\usepackage{graphicx}
\usepackage[T1]{fontenc} % enable hyphenation to operate on texts containing any character in the font
%\usepackage[notref,notcite]{showkeys} % internal key for \label is printed, not for \ref or \cite
\usepackage{cite} % supports compressed, sorted lists of numerical citations
\usepackage{natbib} % implements both author-year and numbered references, as well as much detailed of support for other bibliography use

\usepackage{mathrsfs} % Use of the Raph Smith’s Formal Script font in mathematics. Provides a \mathscr command, rather than overwriting the standard \mathcal command
\usepackage{enumitem} % provides user control over the layout of the three basic list environments: enumerate, itemize and description
\usepackage{tikz-cd} % Create commutative diagrams
\usepackage{mathptmx} % Defines Adobe Times Roman (or equivalent) as default text font
\usepackage{cancel} % draw diagonal lines “cancelling” a term

%\usepackage{esint} % permits access to alternate integral symbols when you are using the Computer Modern fonts
%\usepackage{wrisym}
%\usepackage{amsmidx} %supports the creation and typesetting of multiple of multiple indexes with AMS Classes
%\usepackage{longtable} % Longtable allows you to write tables that continue to the next page
%\usepackage{upref} %The upref package redefines the LATEX \ref command to ensure the numbers are upright, even in an italic or oblique environment.
%\usepackage{xypic} %A package for typesetting a variety of graphs and diagrams with TEX.
%\usepackage{psfrag} % Allows LATEX constructions (equations, picture environments, etc.) to be precisely superimposed over Encapsulated PostScript figures
%\usepackage{epsfig} % Include Encapsulated PostScript in LATEX documents
%\usepackage{pictexwd,dcpic} %typesetting Commutative Diagrams
%\usepackage{showlabels} %keep track of all the labels you define, by putting the name of new labels into the margin whenever the \label command is used.

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

\input{def.tex}


\title{Cuadratura Gaussiana}
\author{Fabricio Macià}


\begin{document}
\maketitle

\noindent\textbf{Objetivo y contexto. }Sea $\Delta=\{x_0,\hdots,x_N\}$ un conjunto de puntos o nodos del intervalo $[a,b]$, distintos dos a dos. Una regla de cuadratura
\[
\cI_\Delta(f):=\omega_0 f(x_0)+\hdots+\omega_N f(x_N),\quad f\in \cC([a,b]),
\]
es de \textit{tipo interpolatorio} si
\[
\cI_\Delta(f)=\int_a^b P_f^\Delta(x)\, dx,
\]
siendo $ P_f^\Delta$ el polinomio interpolador de $f$ por los nodos $\Delta$. Los pesos $(\omega_i)_{i=0,\dots,N}$ de una regla de tipo interpolatorio vienen completamente determinados por los nodos a través de la identidad:
\[
\omega_I=\int_a^b \ell_i(x) \, dx,\quad i=0,\hdots,N,
\]
siendo $\ell_i$ el $i$-ésimo polinomio de la base de Lagrange asociada a $\Delta$.  Una regla de tipo interpolatorio tiene \textit{grado de exactitud} mayor o igual a $\# \Delta-1=N$ (integra de forma exacta todos los polinomios de grado menor o igual a $N$).

Las regla de cuadratura Gaussiana surgen como respuesta a la siguiente pregunta. Fijado $N>0$, ¿es posible elegir una familia $\Delta_ \mathrm{O}$ de $N+1$ nodos de tal manera que el grado de exactitud de $\cI_\Delta$ sea máximo cuando $\Delta = \Delta_ \mathrm{O}$ de entre todas las posibles elecciones de $N+1$ nodos $\Delta$ en $[a,b]$?

\medskip

\noindent\textbf{Una primera caracterización. }Comencemos investigando que condiciones ha de cumplir una familia de $N+1$ nodos $\Delta$ para que
\[
\cI(q)=\int_a^b q(x)\, dx,
\]
para cualquier polinomio $q$ de grado $N+m$, con $m>0$.

Obsérvese que, para un polinomio $q$ de estas características, se cumple que:
\[
q(x)-P_q^\Delta(x) = r(x)(x-x_0)\cdots(x-x_N),
\]
siendo $r$ un polinomio de grado $m-1$. Esto es consecuencia de que el polinomio:
\begin{equation}\label{e:pn1}
p_{N+1}(x):=(x-x_0)\cdots(x-x_N),
\end{equation}
divide a $q-P_q^\Delta$ en el anillo de los polinomios, al ser $\Delta$ raíces de $q-P_q^\Delta$.

Como consecuencia de esta observación, deducimos que si $\cI_\Delta$ tiene grado de exactitud $N+m$ entonces:
\[
0=\int_a^b q(x)\, dx- \cI_\Delta(q)=\int_a^b r(x)p_{N+1}(x)\, dx;
\]
como $q$ es un polinomio arbitrario de grado menor o igual a $N+m$, se sigue que $r$ es un polinomio arbitrario de grado menor o igual a $m-1$. En particular, se tiene que verificar que
\begin{equation}\label{e:opol}
\int_a^b x^kp_{N+1}(x)\, dx=0,\quad k=0,\hdots, m-1.
\end{equation}
Obsérvese que $m-1<N+1$; si $m=N+2$, la identidad \eqref{e:opol} implicaría que
\[
\int_a^b p_{N+1}(x)^2\, dx=0,
\]
lo cual forzaría a que $p_{N+1}=0$, que es contradictorio.

De todo esto, concluimos que
\begin{itemize}
\item Una regla de tipo interpolatorio tiene a lo sumo grado de exactitud $2N+1=2\#\Delta-1$.
\item La existencia de una regla de tipo interpolatorio $\cI_\Delta$ de grado e exactitud $2N+1$ es equivalente a la existencia de un polinomio mónico $p_{N+1}$, de grado $N+1$ y cuyas raíces sean precisamente $\Delta$, que cumpla además \eqref{e:opol} con $m=N+1$.
\end{itemize}

El resto de estas notas está dedicado a mostrar que existe un polinomio de grado $N+1$ con estas propiedades y a dar un procedimiento eficiente para calcular sus raíces, el conjunto $\Delta$ de nodos de las conocidas como \text{reglas de cuadratura Gaussiana}

Es posible además probar que los pesos de las reglas de cuadratura Gaussiana son \text{siempre positivos}. Esto tiene implicaciones prácticas importantes a la hora de minimizar efectos indeseados debidos a la artimética de precisión finita.
\medskip

\noindent\textbf{Polinomios ortogonales. }
La identidad \eqref{e:opol} puede interpretarse como una relación de ortogonalidad dentro del espacio vectorial de los polinomios de grado a lo sumo $N+1$ equipado del producto escalar:
\[
(p\,|\,q):=\int_a^b p(x)q(x)\,dx.
\]

Demostremos la existencia de un polinomio mónico $p_{N+1}$, de grado $N+1$, cuyas raíces sean reales, simples y estén contenidas en $[a,b]$ y que sea ortogonal a todos los polinomios de grado a lo sumo $N$.

Aplicando el algoritmo de Gram-Schmidt a la familia de monomios $\{1,x,\hdots,x^{N+1}\}$ deducimos la existencia de polinomios $p_k$ de grado exactamente $k=0,\hdots,N+1$, con la propiedad de que $p_k$ es ortogonal a todos los polinomios de grado a lo sumo $k-1$. Tras normalización, podemos suponer que cada polinomio $p_k$ es además mónico.

Esto concluye la demostración de existencia de $p_{N+1}$ salvo por la propiedad de localización y multiplicidad de sus raíces, que damos a continuación. Demostraremos que $p_k$ posee $k$ raíces reales simples en $[a,b]$.

Sean $c_1,\hdots,c_r$ las raíces reales de $p_k$ en $[a,b]$ contadas con sus multiplicidades; construyamos
\[
q(x)=(x-c_1)\cdots(x-c_r).
\]
Si $r<k$ entonces $(p_k\,|\,q)=0$; pero por construcción, el siguno de $p_kq$ es constante en $[a,b]$. Esto fuerza a que $r=k$.

Supongamos ahora que alguna raíz es múlltiple. En ese caso existe un índice $i$ y un polinomio $r$ de grado $k-2$ tal que $p_k(x)=(x-c_i)^2r(x)$. En ese caso:
\[
0=\int_a^b p_k(x)r(x)\,dx=\int_a^b(x-c_i)^2r(x)^2 \, dx,
\]
lo cual implicaría la contradicción $0=r=p$.

\medskip

\noindent\textbf{Cálculo de $\Delta_\mathrm{O}$. } Pasemos a calcular $p_{N+1}$. Para ello vamos primero a demostrar la relación de recurrencia:
\begin{equation}\label{e:rec}
p_{k+1}(x)=(x-a_k)p_k(x)-b_k p_{k-1}(x),\quad k=0,\hdots,N,
\end{equation}
siendo $p_{-1}=0$, $b_0=0$, y
\[
a_k:=\frac{\int_a^b xp_k(x)^2\,dx}{\|p_k\|^2},\quad b_k:=\frac{\|p_k\|^2}{\|p_{k-1}\|^2}.
\]
Para ello observamos que
\[
q(x)=p_{k+1}(x)-xp_k(x)
\]
 es un polinomio de grado menor o igual a $k$. Por ello
 \[
0 = (q\,|\, p_{k+1}) = \|p_{k+1}\|^2- \int_a^b x p_k(x)p_{k+1}(x) \, dx,
 \]
 y $q=\alpha_0 p_0+\hdots + \alpha_k p_k$ con
\[
\alpha_i=\frac{(q\,|\, p_{i})}{\|p_i\|^2}.
\]
Calculemos:
\[
(q\,|\, p_{i})=(p_{k+1}\,|\, p_{i})-\int_a^b x p_k(x)p_{i}(x) \, dx=-\int_a^b x p_{i}(x) p_k(x)\, dx.
\]
De aquí se sigue que $\alpha_i=0$ si $i=0,\hdots, k-2$ y:
\[
\alpha_{k-1}=\frac{-\int_a^b x p_{i}(x) p_k(x)\,dx}{\|p_{k-1}\|^2}=-\frac{\|p_k\|^2}{\|p_{k-1}\|^2},
\]
\[
\alpha_k = -\frac{\int_a^b xp_k(x)^2\,dx}{\|p_k\|^2}.
\]
Este procedimiento permite calcular eficientemente $p_{N+1}$; una vez calculado se hallan sus raíces y tenemos así los nodos de la regla de cuadratura Gaussiana.

Este método, no obstante, no es el más eficiente para calcularlos. A continuación, describimos un algoritmo alternativo.
Las identitdades \eqref{e:rec} se pueden escribir en forma vectorial como
\[
\cJ\, P(x) = x P(x) - l(x),
\]
siendo
\[
\cJ:=
\begin{bmatrix}
a_0 & 1     & 0 & \cdots & 0 & 0 & 0\\
b_1 & a_1 & 1 & \cdots & 0 & 0 & 0\\
\vdots & \vdots & & \ddots & \ddots & & \vdots \\
0 & 0 &  0 & \cdots & b_{N-1} & a_{N-1} & 1\\
0 & 0 & 0 & \cdots & 0 & b_N & a_N
\end{bmatrix},
\]
\[
P(x):=
\begin{bmatrix}
p_0(x)\\
p_1(x)\\
\vdots \\
p_{N-1}(x)\\
p_N(x)
\end{bmatrix},
\]
\[
l(x):=
\begin{bmatrix}
0\\
0\\
\vdots \\
0\\
p_{N+1}(x)
\end{bmatrix}.
\]
Por tanto $x_j$ es raíz de $p_{N+1}$ si y sólo si es un autovalor de $\cJ$. Los autovalores de $\cJ$ son fácilmente calculables numéricamente si nos damos cuenta de que $\cJ$ se puede conjugar a una matriz simétrica. Concretamente, si definimos
\[
Q:=\diag(\delta_0,\dots,\delta_N),\quad \delta_0=1,\quad \delta_j=\sqrt{b_1\dots b_j},
\]
Entonces $\tilde{\cJ}:=Q^{-1}\cJ\, Q$ es simétrica y tridiagonal. Las entradas de la diagonal subprincipal son $\tilde{\cJ}_{i+1,i}=\sqrt{b_i}$, y la diagonal coincide con la de $\cJ$. Uno calcula entonces los autovalores de la matriz $\tilde{\cJ}$.

%\bibliography{../../bibliosmc}
%\bibliographystyle{abbrv}


\end{document}
