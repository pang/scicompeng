import matplotlib.pyplot as plt

def plot_triangle(vs, label=''):
    n = len(vs)
    barycenter = vs.mean(axis=0)
    xbarycenter, ybarycenter = barycenter
    plt.plot(vs[:,0],vs[:,1],'o')
    plt.plot(0.98*vs[range(-1,n),0]+0.02*xbarycenter, 0.02*ybarycenter+0.98*vs[range(-1,n),1],'-')
    plt.text(xbarycenter, ybarycenter, 'T%s'%label)
    
def plot_mesh(vertices, ivertices_boundary, triangles):
    for j in range(len(triangles)):
        triangle = triangles[j]
        plot_triangle(vertices[triangle], label=j)

    for j in range(len(vertices)):
        vertex = vertices[j]
        plt.plot(vertex[0], vertex[1], 'ok')
        plt.text(vertex[0]+0.02, vertex[1]+0.02, '%s'%j)
    for j in ivertices_boundary:
        vertex = vertices[j]
        plt.plot(vertex[0], vertex[1], 'ob')