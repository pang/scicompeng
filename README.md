# SciCompEng

Scientific Computing for Engineers is a first course in numerical methods for engineers:

 - Root finding
 - Interpolation and approximation
 - Numerical derivation and quadrature
 - Fourier transform
 - Integration of systems of ODEs
 - Introduction to the numerical integration of PDEs

## Course material

 - A cheatsheet with a summary of the theory and the relevant `python` commands.
 - `jupyter` notebooks with theory and examples.
 - exams

## Language

  - This course has been taught for several years at [ETSI Navales](https://www.etsin.upm.es) (Naval Engineering) at [UPM](https://www.upm.es) in spanish, and the full material is available.
  - We are in the course of translating the jupyter notebooks.

## License

This work is licensed under a [`GFDL`](http://www.gnu.org/copyleft/fdl.html) license, or a [`Creative Commons Attribution-Share Alike 3.0 License`](http://creativecommons.org/licenses/by/3.0/), at your choice. All our original materal is also distributed under a [`cc-by-nc`](http://creativecommons.org/licenses/by-nc/3.0/) license, with the possible exception of a few images which are not original and came with an incompatible license, which will be listed explicitely.
