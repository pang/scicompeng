def myfun(x):
    return np.sin(x**2 + 1)

xmin, xmax = -3, 3 #plotting interval
N = 100 # number of subdivions
xs = np.linspace(xmin, xmax, N)
ys = myfun(xs)
plt.plot(xs, ys, 'g')
