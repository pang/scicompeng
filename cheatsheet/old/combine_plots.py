fun1 = np.exp
fun2 = lambda xs: np.exp(xs+1)

xmin, xmax = -1,8
N = 100
xs = np.linspace(xmin, xmax, N)
ys1 = fun1(xs)
plt.plot(xs, ys1, 'g', label='exp(x)')

ys2 = fun2(xs)
plt.plot(xs, ys2, 'b-', label='exp(x+1)')

plt.title('Plot of two functions')
plt.xlabel('x')
plt.legend()
plt.show()
