\documentclass[10pt,landscape]{article} \usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{graphicx}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{latexsym, marvosym}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{graphicx}
\usepackage{lipsum}
\usepackage{array}
\usepackage{booktabs}
\usepackage[bottom]{footmisc}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{pdfpages}
\usepackage{wrapfig}
\usepackage{enumitem}
\setlist[description]{leftmargin=0pt}
\usepackage{xfrac}
% Style and layout from \url{http://github.com/wzchen/probability_cheatsheet}
\usepackage[pdftex,
            pdfauthor={Pablo Angulo, Fabricio Macià},
            pdftitle={python and MILP Cheatsheet},
            pdfsubject={A cheatsheet pdf and reference guide covering basic python commands by Pablo Angulo and Fabricio Macià for ETSIN@UPM},
            pdfkeywords= {numerical methods} {root finding} {interpolation} {python} {programming} {numpy} {scipy} {pdf} {cheat} {sheet}
            ]{hyperref}
\usepackage[
            open,
            openlevel=2
            ]{bookmark}
\usepackage{relsize}
\usepackage{rotating}

\usepackage{color}
\definecolor{gray95}{gray}{.95}
\definecolor{gray75}{gray}{.75}
\definecolor{gray45}{gray}{.45}

\usepackage{listings}
\definecolor{keywords}{RGB}{48,48,255}
\definecolor{comments}{RGB}{70,180,130}
\definecolor{strings}{RGB}{30,200,60}
\definecolor{gray95}{RGB}{245,245,245}
\lstset{ frame=Ltb,
     framerule=0pt,
     aboveskip=0.3cm,
     framextopmargin=1.5pt,
     framexbottommargin=1.5pt,
     framexleftmargin=0.3cm,
     framesep=0pt,
     rulesep=0pt,
%     backgroundcolor=\color{gray95},
     rulesepcolor=\color{black},
     %
     stringstyle=\ttfamily,
     showstringspaces = false,
     basicstyle=\scriptsize\ttfamily,
     commentstyle=\color{comments}\emph,
     keywordstyle=\bfseries\color{keywords},
     stringstyle=\color{strings},
     %
     numbers=none,
     numbersep=12pt,
     numberstyle=\tiny,
     numberfirstline = false,
     breaklines=true,
	%
	language=Python
	}

% minimizar fragmentado de listados
\lstnewenvironment{listing}[1][]
   {\lstset{#1}\pagebreak[0]}{\pagebreak[0]}

\lstdefinestyle{consola}
   {basicstyle=\scriptsize\bf\ttfamily,
    backgroundcolor=\color{gray75},
   }


 \newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
    \def\independenT#1#2{\mathrel{\setbox0\hbox{$#1#2$}%
    \copy0\kern-\wd0\mkern4mu\box0}}

\newcommand{\R}{\mathbb{R}}
\newcommand{\noin}{\noindent}
\newcommand{\logit}{\textrm{logit}}
\newcommand{\var}{\textrm{Var}}
\newcommand{\cov}{\textrm{Cov}}
\newcommand{\corr}{\textrm{Corr}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\DisUniform}{\textrm{DisUniform}}
\newcommand{\Bern}{\textrm{Bern}}
\newcommand{\Bin}{\textrm{Bin}}
\newcommand{\Beta}{\textrm{Beta}}
\newcommand{\Gam}{\textrm{Gamma}}
\newcommand{\IGam}{\textrm{InverseGamma}}
\newcommand{\Expo}{\textrm{Expo}}
\newcommand{\Pois}{\textrm{Pois}}
\newcommand{\Unif}{\textrm{Unif}}
\newcommand{\Geom}{\textrm{Geom}}
\newcommand{\NBin}{\textrm{NBin}}
\newcommand{\Hypergeometric}{\textrm{HGeom}}
\newcommand{\HGeom}{\textrm{HGeom}}
\newcommand{\Mult}{\textrm{Mult}}

\geometry{top=.4in,left=.2in,right=.2in,bottom=.4in}

\pagestyle{empty}
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

% -----------------------------------------------------------------------

\usepackage{titlesec}

\titleformat{\section}
{\color{blue}\normalfont\large\bfseries}
{\color{blue}\thesection}{1em}{}
\titleformat{\subsection}
{\color{cyan}\normalfont\normalsize\bfseries}
{\color{cyan}\thesection}{1em}{}
% Comment out the above 5 lines for black and white

\begin{document}

\raggedright
\footnotesize
\begin{multicols*}{3}

% multicol parameters
% These lengths are set only within the two main columns
%\setlength{\columnseprule}{0.25pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% TITLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}
    {\color{blue} \Large{\textbf{SciCompForEngineers Cheatsheet}}}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ATTRIBUTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\scriptsize

Pablo Angulo and Fabricio Macià for ESTIN@UPM.ES \href{http://github.com/wzchen/probability_cheatsheet}{(layout by wzchen)}
\begin{figure}[!ht]
  \begin{tabular}{cc}
    \includegraphics[width=1cm]{figures/upm.jpg} &
    \includegraphics[width=1cm]{figures/LOGOETSIN.pdf}
  \end{tabular}
\end{figure}



% \begin{center}
%     Last Updated \today
% \end{center}

% Cheatsheet format from
% http://www.stdout.org/$\sim$winston/latex/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% BEGIN CHEATSHEET
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Errors}\smallskip \hrule height 2pt \smallskip
\subsection{Types of error}
\begin{description}
  \item[Measurement error] Noise, imprecision of measuring instrument, etc
  \item[Model error] Our model is a simplification of the real word
  \item[Truncation error] Replace a complicated or unknown function by a polynomial, etc
  \item[Rounding error] Represent real numbers with finite precision, and perform computations with the approximations instead of the original numbers.
\end{description}
\subsection{Big O}
\subsubsection{$f(x) =  O(g(x))$ when $x \rightarrow a$}
$f(x) =  O(g(x))$ when $x \rightarrow a$
$\Leftrightarrow$
$|f(x)| \leq M |g(x)|$  when $|x - a| < \delta$, for some $\delta,M>0$.
If $a=0$, $\Delta x$ is small, and:
$$\begin{aligned}
    f(\Delta x) &= p(\Delta x) + O(\Delta x^n) \\
    g(\Delta x) &= q(\Delta x) + O(\Delta x^m) \\
    r &= \min(n, m)
\end{aligned}$$
then
\begin{itemize}
  \item
  $f+g = p + q + O(\Delta x^r)$
  \item
  $f \cdot g = p \cdot q + p \cdot O(\Delta x^m) + q \cdot O(\Delta x^n) + O(\Delta x^{n+m}) = p \cdot q + O(\Delta x^r)$
\end{itemize}

\subsubsection{$f(x) =  O(g(x))$ when $x \rightarrow \infty$}
$f(x) =  O(g(x))$ when $x \rightarrow \infty$
$\Leftrightarrow$
$|f(x)| \leq M |g(x)|$  when $x > K$, for some $K,M>0$.
If
$$\begin{aligned}
    f(x) &= p(x) + O(x^n) \\
    g(x) &= q(x) + O(x^m) \\
    k &= \max(n, m)
\end{aligned}$$
\begin{itemize}
  \item $f+g = p + q + O(x^k)$
  \item $f \cdot g = p \cdot q + p O(x^m) + q O(x^n) + O(x^{n + m})= p \cdot q + O(x^{n+m})$
\end{itemize}
\subsection{Taylor theorem}
$$
f(x) = \sum^N_{n=0} \frac{f^{(n)}(x_0)\cdot(x-x_0)^n}{n!} + O(x^{n+1})
$$
\subsection{Horner's nested evaluation}
In order to evaluate $f(x)=a_0+a_1\:x+\dots+a_n\:x^n$ at $x=x_0$, place parenthesis like this:
$$
f(x) = a_0 + x\cdot\left(a_1 + x\cdot\left(a_2 + \dots + x\cdot a_n \right) \right)
$$
This reduces computing time and rounding error.
\begin{lstlisting}[language=python]
# coefs = [a0,a1,a2,...,an]
def Horner(x0, coefs):
    r = coefs[-1]
    for a in reversed(coefs[:-1]):
        r = r*x0 + a
    return r
\end{lstlisting}


\subsection{Floating point numbers}
32-bit floating point
\begin{itemize}
  \item One bit for the sign
  \item 8 bits for exponent
  \item 23 bits for mantissa
\end{itemize}
\begin{minipage}{\linewidth}
    \includegraphics[width=8cm]{figures/floating_point_layout.png}
\end{minipage}

\

Intuition:
\begin{itemize}
  \item
The exponent chooses a window between two consecutive powers of 2: $[2^s,2^{s+1} ]$.
\item The mantissa choose one of $2^{23}$ points regularly spaced in the interval $[2^s,2^{s+1}]$.
\href{https://fabiensanglard.net/floating_point_visually_explained/index.html}{[1]}
\end{itemize}

\

\begin{minipage}{\linewidth}
    \includegraphics[width=8cm]{figures/floating_point_window.png}
\end{minipage}


\section{Root finding with \texttt{scipy.optimize}}\smallskip \hrule height 2pt \smallskip
Goal: Given $f:\mathbb{R}\rightarrow \mathbb{R}$, find $c\in\mathbb{R}$ st $f(c)=0$.
\subsection{Plot a function and find a root}

\begin{lstlisting}[language=python]
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import bisect
def f(x):
    return x**3 - 4*np.sin(x) - 1
a, b = -2, 2
x0 = bisect(f, a, b)
xs = np.linspace(a-1,b+1,100) # regularly spaced points
plt.plot(xs,f(xs))            # draw f
plt.axhline(color='k')        # draw x axis
plt.plot([a, b], [0,0], 'o')  # initial interval
plt.plot([x0], [0], 'o')      # root
\end{lstlisting}

% \subsection{import modules}
% \begin{lstlisting}[language=python]
% # import all the module
% import scipy.optimize as opt
% # import particular methods
% from scipy.optimize import bisect, newton
% \end{lstlisting}

\subsection{Bisection method}
\begin{itemize}
  \item A real-valued continuous function $f$ defined on an interval $[a,b]$
  \item The signs of $f(a)$ and $f(b)$ are different $\Rightarrow$ by Bolzano theorem there is a root $c\in[a,b]$ st $f(c)=0$.
  \item Guaranteed to find one root, but may pick any if there is more than one.
  \item Guaranteed to achieve precision $\frac{b-a}{2^n}$ after $n$ iterations.
\end{itemize}
\begin{description}
  \item[INIT] Start with \texttt{interval = [a,b]}
  \item[REPEAT] \texttt{c=(a+b)/2}. If \texttt{sign(f(a))!=sign(f(c))}, then set \texttt{interval = [a,c]}, otherwise it must hold that \texttt{sign(f(c))!=sign(f(b))}, and we set \texttt{interval = [c,b]}.
  \item[UNTIL] Repeat until length of \texttt{interval} is smaller than \texttt{xtol}.
\end{description}

\begin{itemize}
  \item Find an approximation to the root
\begin{lstlisting}[language=python]
x0 = bisect(f, a, b)
\end{lstlisting}
\item Outputs information about convergence
\begin{lstlisting}[language=python]
x0, extra = bisect(f, a, b, full_output=True)
\end{lstlisting}
\end{itemize}
\begin{minipage}{\linewidth}
    \includegraphics[width=8cm]{figures/bisection.png}
\end{minipage}

\subsection{Secant method}
\begin{itemize}
  \item A function $f$ with a simple root $x_0$ (e.g. $f'(x_0)\neq 0$).
  \item Convergence is faster than the bisection method.
  \item Generalizes to higher dimensions (\emph{Broyden's method}).
  \item Needs a good initial approximation.
  \item Does not need the first derivative of $f$.
\end{itemize}
\begin{description}
  \item[INIT] Start with two approximations to the root \texttt{x0,x1}
  \item[REPEAT] Compute a new point \texttt{x2 = x1 - f(x1)*(x1-x0)/(f(x1)-f(x0))} (root of the linear function through $(x_0,f(x_0))$ and   $(x_1,f(x_1))$). Then advance the indices \texttt{x0,x1=x1,x2}.

  \item[UNTIL] Two stop criterion are common (choose one):
    \begin{description}
      \item[x-tolerance]
      Repeat until \texttt{np.abs(x1-x0)} is smaller than \texttt{xtol}.
      \item[y-tolerance]
      Repeat until \texttt{np.abs(f(x1))} is smaller than \texttt{ytol}.
    \end{description}
\end{description}

\begin{minipage}{\linewidth}
    \includegraphics[width=8cm]{figures/secant.png}
\end{minipage}
\begin{itemize}
  \item Find an approximation to the root
\begin{lstlisting}[language=python]
from scipy.optimize import newton
xroot = newton(f, x0=xfirst, x1=xsecond)
\end{lstlisting}
\item Find an approximation to the root, makes up xsecond if not provided
\begin{lstlisting}[language=python]
xroot = newton(f, x0=xfirst)
\end{lstlisting}
\item Outputs information about convergence
\begin{lstlisting}[language=python]
xroot, extra = newton(f, x0=xfirst, x1=xsecond, full_output=True)
\end{lstlisting}
\end{itemize}

\subsection{Newton method}
\begin{itemize}
  \item A function $f$ with a simple root $x_0$ (e.g. $f'(x_0)\neq 0$).
  \item Convergence is quadratic.
  \item Generalizes to higher dimensions.
  \item Needs a good initial approximation.
  \item Needs the first derivative of $f$.
\end{itemize}
\begin{description}
  \item[INIT] Start with one approximations to the root \texttt{x0}
  \item[REPEAT] Compute a new point \texttt{x1 = x0 - f(x0)/f'(x1)} (root of the linear function through $(x_0,f(x_0))$ with slope $f'(x_1)$. Then advance the indices \texttt{x1=x0}.
  \item[UNTIL] Two stop criterion are common (choose one):
    \begin{description}
      \item[x-tolerance]
      Repeat until \texttt{np.abs(x1-x0)} is smaller than \texttt{xtol}.
      \item[y-tolerance]
      Repeat until \texttt{np.abs(f(x1))} is smaller than \texttt{ytol}.
    \end{description}
\end{description}

\begin{minipage}{\linewidth}
    \includegraphics[width=8cm]{figures/newton.png}
\end{minipage}
\begin{itemize}
  \item Find an approximation to the root, \texttt{fprime} was computed by hand :-/
\begin{lstlisting}[language=python]
xroot = newton(f, x0=xfirst, fp=fprime)
\end{lstlisting}
\item Use \texttt{sympy} to compute derivative of \texttt{f}:
\begin{lstlisting}[language=python]
import sympy as sym
x = sym.symbols('x') # define a symbol
# y is a symbolic function
y = 1 + (x**3 - 4*x) + sym.log(1+x**2)
# derivative of y with respect to x
yder = sym.diff(y,x)
# lambdify builds a python function that accepts numpy arrays
f = sym.lambdify(x, y)
fp = sym.lambdify(x, yder)
xroot = newton(f, x0, fp=fp)
\end{lstlisting}
\item Outputs information about convergence
\begin{lstlisting}[language=python]
x0, extra = newton(f, x0, fp, full_output=True)
\end{lstlisting}
\end{itemize}
\section{Finding roots in higher dimension}
\begin{lstlisting}[language=python]
from scipy.optimize import root
def F(xs):
    x,y=xs
    return y + np.log(x), x-np.sin(y)
output = root(F, [1,1]) # contains root and convergence information
F(output['x']) # output['x'] is a root => F(output['x']) is almost zero
\end{lstlisting}
\section{Interpolation}
\subsection{Interpolating polynomial}
\subsubsection{Definition}
The \emph{interpolating polynomial} of $f$ through points $(x_0,y_0),\dots (x_n,y_n)$, where the $x_i$ \emph{are all different}, is the \emph{unique} polynomial $P$ of degree $\leq n$ such that $P(x_i)=y_i$ for $i=0,1,\dots,n$.
\subsubsection{Error}
When $y_i=f(x_i)$ for a $(N+1)$-times differentiable function:
$$
\text{Error}=f(x)-P(x)=\frac{(x-x_0)(x-x_1)\dots (x-x_N)}{(N+1)!}f^{(N+1)}(\xi_x),
$$
for some unknown point $\xi_x$ in the interval of the points $(x_i)$.
\subsubsection{Vandermonde matrix}
The coefficients $\overline{a}=(a_i)_{i=0}^n$ of $P$ satisfy a linear system of equations:
$$
\begin{array}{rl}
a_0+a_1\,x_0+\dots+a_n\,x_0^n&=y_0
\\
a_0+a_1\,x_1+\dots+a_n\,x_1^n&=y_1,
\\
&\vdots
\\
a_0+a_1\,x_n+\dots+a_n\,x_n^n&=y_n.
\end{array}
$$
or $V\cdot \overline{a} = \overline{y}$, for the Vandermonde matrix $V$ of the points $(x_i)$.
\subsubsection{Lagrange form of the interpolating polynomial}
$$
P(x):=y_0\ell _{0}(x)+y_1\ell _{1}(x)+\dots+y_N\ell _{N}(x).
$$
where
$$
\ell _{j}(x):=\prod _{\begin{smallmatrix}0\leq m\leq N\\m\neq j\end{smallmatrix}}{\frac {x-x_{m}}{x_{j}-x_{m}}};
$$
\subsubsection{Newton's form}
$$
\begin{array}{rl}
P(x)=&f[x_{0}]+f[x_{0},x_{1}](x-x_{0})+\dots\\
   &+f[x_{0},\ldots ,x_{N}](x-x_{0})(x-x_{1})\dots (x-x_{{N-1}}).
\end{array}
$$
where $f[x_{m },\ldots ,x_{{m +j}}]$ are the \emph{divided differences}, defined for $j=1,\ldots ,N$ and $m = 0,\ldots ,N-j$ as:
$$
f[x_{m },\ldots ,x_{{m +j}}]:={\frac  {f[x_{{m +1}},\ldots ,x_{{m +j}}]-f[x_{m },\ldots ,x_{{m +j-1}}]}{x_{{m +j}}-x_{m }}}.
$$
and for $m = 0,\ldots ,N$:
$$
f[x_{m }]:=f(x_{{m}}),\qquad m=0,\ldots ,N
$$
\subsubsection{\texttt{numpy}'s \texttt{polyfit} and \texttt{polyval}}
\begin{description}
  \item[\texttt{polyfit}] Returns the coefficients of the polynomial $P$ of degree $k$ that minimizes squared error between $f$ and $P$ through points \texttt{xs} (an approximation polynomial).
\begin{lstlisting}[language=python]
ys = f(xs)
coefs = np.polyfit(xs, ys, k)
\end{lstlisting}
\item[\texttt{polyfit}] If $k$ is the number of points minus 1, then \texttt{polyfit} actually computes the interpolating polynomial:
\begin{lstlisting}[language=python]
coefs = np.polyfit(xs, ys, len(xs)-1)
\end{lstlisting}
\item[\texttt{polyval}] Evals a polynomial on a set of points \texttt{xeval}, where the polynomial is given by its coefficients \texttt{coefs}.
\begin{lstlisting}[language=python]
xeval = np.linspace(-1,1,100) # 100 evenly spaced points, for plotting
yeval = np.polyval(coefs, xeval)
\end{lstlisting}

\end{description}
\subsubsection{Chebyshev nodes}
The interpolating polynomial does not need the points to be evenly spaced.
Indeed, \emph{evenly spaced points can lead to large errors} (Runge's phenomenon).
This can be avoided if nodes are chosen carefully. For instance, Chebyshev nodes minimize the error. For the interval $[-1,1]$, the nodes are:

$$
    x_k = \cos\left( \frac{(2k - 1) \pi}{2 N} \right ) \quad \text{for} \quad k=1, \ldots, N
$$
\begin{minipage}{\linewidth}
    \includegraphics[width=8cm]{figures/runge.png}
\end{minipage}

\subsection{Hermite interpolation}
Given $(n+1)$ different points $x_0,\dots,x_n$, we look for a polynomial $P(x)$ of degree $\leq 2n+1$ satisfying:
$$
P(x_0)=y_0,\dots, P(x_n)=y_n
$$
$$
P'(x_0)=z_0,\dots, P'(x_n)=z_n.
$$
\begin{itemize}
  \item Theory is analogous to that for interpolating polynomials.
  \item It is possible to define Hermite polynomial interpolators that fit derivatives of degree higher than one.
\end{itemize}
\subsection{Piecewise linear interpolation (Linear Splines)}
The linear spline $s_n$ interpolating through $(x_j, y_j)_{j=0}^n$ is defined piecewise: a linear (degree one polynomial) on each $[x_{j},x_{j+1}]$ that interpolates the given values.
 
\begin{itemize}
  \item This forces, for $x$ in $[x_{j},x_{j+1}]$:
  $$s_n(x) = y_j+ \frac{y_{j+1}-y_j}{x_{j+1}-x_j}(x-x_j).$$
  \item When $y_j=f(x_j)$  the error satisfies:
  $$|f(x)-s_n(x)|\leq \frac{h^2}{8}\max_{[x_0,x_n]}|f''(x)|,$$
  where
  $$h=\max_{i=0,\dots,N-1}|x_{i+1}-x_i|.$$
  \item Create an \texttt{UnivariateSpline} object interpolating piecewise linearly points with $x$    		coordinates \texttt{xs} and $y$-coordinates \texttt{ys}:
\begin{lstlisting}[language=python]
from scipy.interpolate import UnivariateSpline
s = UnivariateSpline(xs, ys, k=1, s=0)
\end{lstlisting}
\end{itemize}

\subsection{Cubic splines}
The cubic spline interpolating through $(x_j, y_j)_{j=0}^n$ is defined piecewise: a cubic polynomial $S_j$ in each interval $[x_{j},x_{j+1}]$ such that:
\begin{itemize}
  \item It interpolates the given values (this implies that $S$ is continuous)
  $$S_j(x_j) = y_j,\; S_j(x_{j+1}) = y_{j+1}$$
  \item It has a continuous first derivative. It is sufficient to check at the nodes:
  $$S_j'(x_j) = S_{j+1}'(x_j)$$
  \item It has a continuous second derivative. It is sufficient to check at the nodes:
  $$S_j''(x_j) = S_{j+1}''(x_j)$$
\end{itemize}
The above amounts to $4n-2$ conditions, for a total of $4n$ degrees of freedom (4 for each interval $[x_{j},x_{j+1}]$).
So there are two missing conditions, and there are several alternatives:
\begin{description}
  \item[Natural boundary conditions]
$S_0''(x_0)=0$, $S_n''(x_n)=0$
\item[Clamped boundary conditions]
$S_0'(x_0)=0$, $S_n'(x_n)=0$
\item[`Not-a-knot' boundary conditions]
$S_0'''(x_1)=S_1'''(x_1)$, $S_{n-1}'''(x_{n-1})=S_{n}'''(x_{n-1})$.
\end{description}
\begin{itemize}
  \item Create a \texttt{CubicSpline} object interpolating points with $x$-coordinates \texttt{xs} and $y$-coordinates \texttt{ys}:
\begin{lstlisting}[language=python]
from scipy.interpolate import CubicSpline
cs3 = CubicSpline(xs, ys)
\end{lstlisting}
\item Use different boundary conditions (default is \emph{'not-a-knot'})
\begin{lstlisting}[language=python]
from scipy.interpolate import CubicSpline
cs3 = CubicSpline(xs, ys, bc_type='natural')
\end{lstlisting}
\item Eval a \texttt{CubicSpline} on a set of points \texttt{xeval}.
\begin{lstlisting}[language=python]
# 200 evenly spaced points, for plotting
xeval = np.linspace(min(xs), max(xs), 200)
# eval the cubic spline on xeval, and plot
plt.plot(xeval, cs3(xeval))
\end{lstlisting}
\end{itemize}

\end{multicols*}
\end{document}
